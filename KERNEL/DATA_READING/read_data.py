import os
from sklearn.model_selection import train_test_split
import itertools
import numpy

def find_list_of_files(data_folder):
    list=[]
    for x in os.listdir(data_folder):
        if (x == '.DS_Store') is not True:
            list.append(x)
    list.sort()
    print('Length of the datset in ', data_folder ,' is:', len(list))
    return list


def make_dictionary_of_all_dataset_filenames(dataset_directory, input_directory,
                                             output_directory, number_of_samples=None):

    all_dataset_filenames = {}
    input_directory = dataset_directory + '/' + input_directory + '/'
    input_filenames = find_list_of_files(input_directory)
    output_directory = dataset_directory + '/' + output_directory + '/'
    output_filenames = find_list_of_files(output_directory)

    for ifile in range(len(input_filenames)):
        all_dataset_filenames[ifile] = {'input_file': input_filenames[ifile],
                                        'output_file': output_filenames[ifile]}

    if number_of_samples is not None:
       all_dataset_filenames = dict(itertools.islice(all_dataset_filenames.items(), number_of_samples))

    return all_dataset_filenames

def convert_dictionary_to_list(dictionary_filenames):
    list_filenames_input = []
    list_filenames_output = []
    for ifile in range(len(dictionary_filenames)):
        list_filenames_input.append(dictionary_filenames[ifile]['input_file'])
        list_filenames_output.append(dictionary_filenames[ifile]['output_file'])
    return list_filenames_input, list_filenames_output

def split_data_to_train_validation_test(dataset_dictionary, percentage_train, percentage_val,
                                        percentage_test, random_state):

    # divide the dataset into train, validation, and test datasets
    if (percentage_train == 1.0):
        input_training, output_training = convert_dictionary_to_list(dataset_dictionary)
        input_val, output_val, input_test, output_test = [], [], [], []

    else:
        dataset_train, dataset_val_test = train_test_split(dataset_dictionary, random_state=random_state,
                                                           test_size=1.0 - percentage_train)

        dataset_val, dataset_test = train_test_split(dataset_val_test, random_state=random_state,
                                                     test_size=percentage_test / (percentage_val + percentage_test))

        input_training, output_training = convert_dictionary_to_list(dataset_train)

        # list of the names for the validation
        input_val, output_val = convert_dictionary_to_list(dataset_val)

        # list of the names for the test
        input_test, output_test = convert_dictionary_to_list(dataset_test)

    return input_training, output_training, input_val, output_val, input_test, output_test



def load_all_files(data_directory, filenames, dataset_size, sample_dim, delimiter=None, skip_header=0):

    # initiate numpy array
    dataset_dim = [dataset_size]
    dataset_dim[1:] = sample_dim[:]
    data = numpy.empty(dataset_dim)

    # reading each file
    for i in range(dataset_size):
        print('Load file', i, '-->', filenames[i])
        data_file = data_directory + filenames[i]
        data[i, ] = numpy.genfromtxt(data_file, dtype=float, delimiter=delimiter,
                                     skip_header=skip_header, max_rows=sample_dim[0])

    return data

def load_all_data(dataset_directory, dataset_size, input_filenames,input_directory, input_dim,
                  output_filenames, output_directory, output_dim, delimiter,
                  skip_header_input=0, skip_header_output=0):
    input_directory = dataset_directory + '/' + input_directory + '/'
    inputs = load_all_files(data_directory=input_directory, filenames=input_filenames, dataset_size=dataset_size,
                          sample_dim=input_dim, delimiter=delimiter, skip_header=skip_header_input)
    output_directory = dataset_directory + '/' + output_directory + '/'
    outputs = load_all_files(data_directory=output_directory, filenames=output_filenames, dataset_size=dataset_size,
                           sample_dim=output_dim, delimiter=delimiter, skip_header=skip_header_output)
    return inputs, outputs

def load_train_val_test(dataset_directory, percentage_train, percentage_val, percentage_test, input_dim, output_dim,
                        dataset_length=None, random_state=1, input_directory='INPUT',
                        output_directory='OUTPUT', delimiter=None,
                        skip_header_input=0, skip_header_output=0):

    print('********************************************************************')
    print('Start Loading data')
    print('********************************************************************')

    all_dataset_filenames = make_dictionary_of_all_dataset_filenames(dataset_directory=dataset_directory,
                                                                      input_directory=input_directory,
                                                                      output_directory=output_directory,
                                                                      number_of_samples=dataset_length)
    # truncate the dataset into train, validation, and test datasets
    input_train_list, output_train_list, input_val_list, output_val_list, \
    input_test_list, output_test_list= split_data_to_train_validation_test(dataset_dictionary=all_dataset_filenames,
                                                                           percentage_train=percentage_train,
                                                                           percentage_val=percentage_val,
                                                                           percentage_test=percentage_test,
                                                                           random_state=random_state)

    # load the training dataset
    print('\033[35m'+'Load training dataset:'+'\33[0m')
    input_train, output_train = load_all_data(dataset_directory=dataset_directory,
                                              dataset_size=len(input_train_list),
                                              input_filenames=input_train_list,
                                              input_directory=input_directory,
                                              input_dim=input_dim,
                                              output_filenames=output_train_list,
                                              output_directory=output_directory,
                                              output_dim=output_dim,
                                              delimiter=delimiter,
                                              skip_header_input=skip_header_input,
                                              skip_header_output=skip_header_output)

    # load the validation dataset
    print('\033[35m'+'Load validation dataset:'+'\33[0m')
    input_val, output_val = load_all_data(dataset_directory=dataset_directory,
                                          dataset_size=len(input_val_list),
                                          input_filenames=input_val_list,
                                          input_directory=input_directory,
                                          input_dim=input_dim,
                                          output_filenames=output_val_list,
                                          output_directory=output_directory,
                                          output_dim=output_dim,
                                          delimiter=delimiter,
                                          skip_header_input=skip_header_input,
                                          skip_header_output=skip_header_output)

    # load the test dataset
    print('\033[35m'+'Load test dataset:'+'\33[0m')
    input_test, output_test = load_all_data(dataset_directory=dataset_directory,
                                            dataset_size=len(input_test_list),
                                            input_filenames=input_test_list,
                                            input_directory=input_directory,
                                            input_dim=input_dim,
                                            output_filenames=output_test_list,
                                            output_directory=output_directory,
                                            output_dim=output_dim,
                                            delimiter=delimiter,
                                            skip_header_input=skip_header_input,
                                            skip_header_output=skip_header_output)

    print('********************************************************************')
    print('Finish Loading data')
    print('********************************************************************')

    return input_train, output_train, input_val, output_val, input_test, output_test


    #
def read_header_from_file_and_create_list(folder_of_file, number_output_columns, header_delimiters_list, string_list_to_delete):
    one_filename = os.listdir(folder_of_file)[0]
    with open(folder_of_file+"/"+one_filename) as f:
        first_line = f.readline()
    #remove unwanted characters
    first_line = first_line.replace('/',"_")
    first_line = first_line.replace("\r","")
    first_line = first_line.replace("\n","")
    for i in string_list_to_delete:
        first_line = first_line.replace(i, "")
    #generate a list from the header string

    counter = 0
    header = [None] * number_output_columns
    for char in first_line:
        if char == " ": pass
        elif char in header_delimiters_list:
            counter = counter + 1
        else:
           if header[counter] is None:
               header[counter]= str(char)
           else:
               header[counter] +=  str(char)
    return header