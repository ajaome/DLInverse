# -*- coding: utf-8 -*-
import numpy
import shutil
import os
import tensorflow as tf
#import keras.backend.tensorflow_backend as K
from tensorflow.keras.utils import plot_model
#
from GENERATE_RESULTS.utilities import make_results_directories
#from GENERATE_RESULTS.generate_graphics import plot_ground_truth_vs_predicted_all_data_partitions
#from DNN_TRAINING.training_tools import  save_history_draw_loss
#from DNN_TRAINING.save_load_model import save_weights_and_optimizer
from DNN_TRAINING.save_load_model import save_architecture_json, load_weights_and_optimizer, load_architecture,remove_old_files, search_last_epoch
#from GENERATE_RESULTS.generate_graphics import plot_errors
#from GENERATE_RESULTS.generate_tex import generate_tex
#we have to eliminate get_custom_objects()
#from DNN_TRAINING_USER.my_model import get_custom_objects


def generate_and_train_model(experiment_name_base,NN_input, NN_output, model_function_variables, model_var, train_var, report, results_path):
     
    numpy.random.seed(train_var.random_seed)
    tf.random.set_seed(train_var.random_seed)

    for model_name, mymodel_func ,optimizer_name, norm_type , loss, loss_weights, metrics in\
        zip(model_var.name,model_var.function, model_var.optimizer, model_var.norm_type, model_var.loss, model_var.loss_weights, model_var.metrics):

        #define name and path in results
        experiment_name = experiment_name_base + '_%s' % (model_name)

        output_path = results_path + '/' + experiment_name + '_dataset%d' % train_var.number_samples         

        # make the directories for the experiment and results 
        make_results_directories(output_path)
        # Initialize the names of the files to save
#        model_folder = output_path + '/model_files/h5/' + experiment_name
        
        #copy the scalers inside the experiment folder
        #this would cause problems needs to be done properly
        try:
            shutil.copytree("./scalers",  output_path +  '/model_files/scalers')
            print('copy scalers in: '+ output_path +  '/model_files/scalers')
        except:
            print('scalers exist in: '+ output_path +  '/model_files/scalers')
        #######################################################################             
        #build the model (and submodels)
        my_model = mymodel_func(model_function_variables)
        my_model.compile(optimizer=train_var.optimizers[optimizer_name],loss=loss,loss_weights=loss_weights, metrics=metrics,experimental_run_tf_function=False)
        # TODO uncoment and push to master

        #try to load the weights and optimizer state for retraining
        try: 
            if (train_var.retrain_from_epoch == "Last" and len(os.listdir(output_path+'/model_files/h5')) != 0):
                #search the last epoch
                print ('*-*****search')
                train_var.retrain_from_epoch = search_last_epoch(output_path, experiment_name)
                print("Last epoch = ", train_var.retrain_from_epoch )
            
            my_model=load_weights_and_optimizer(my_model, experiment_name, output_path, train_var.retrain_from_epoch)
            #my_model.load_weights(model_folder + '_weights_epoch_' +str(train_var.retrain_from_epoch)+'.h5')
            print('Weights and optimizer loaded at epoch '+ str(train_var.retrain_from_epoch))
            print()
            remove_old_files(output_path,experiment_name,train_var.retrain_from_epoch)            
            start_epoch= train_var.retrain_from_epoch + 1
            print()         
        except:
            start_epoch = 0
            #To do: remove_all_files(output_path + '/model_files/h5')
            #
            #plot a sketch of the model once
#            plot_model(my_model, to_file=output_path+'/model_files/model_plot.png', show_shapes=True, show_layer_names=True)
#            print()

        #************************************************************************************
 #think about which variables to append (for callbacks)
        my_model.x_train = NN_input.train
        my_model.y_train = NN_output.train
        my_model.x_validation = NN_input.validation
        my_model.y_validation = NN_output.validation
        my_model.x_names = NN_input.names
        my_model.y_names = NN_output.names
#        my_model.filename = model_folder
        my_model.experiment_folder = output_path
        my_model.experiment_name = experiment_name
        my_model.start_epoch = start_epoch
        my_model.save_each_n_epoch = train_var.save_each_n_epoch
        #************************************************************************************
        # train
        my_model.fit(x=NN_input.train, 
                     y=NN_output.train,
                     batch_size=train_var.batch_size, 
                     epochs=train_var.number_epochs+start_epoch,
                     verbose = train_var.verbose,
                     callbacks=train_var.callbacks,
                     validation_split= train_var.validation_split,
                     validation_data=(NN_input.validation, NN_output.validation),
                     shuffle=train_var.shuffle,
                     class_weight=train_var.class_weight,
                     sample_weight=train_var.sample_weight,
                     initial_epoch = start_epoch,
                     steps_per_epoch=train_var.steps_per_epoch,
                     validation_steps=train_var.validation_steps,
                     validation_freq=train_var.validation_freq,
                     max_queue_size=train_var.max_queue_size,
                     workers=train_var.workers,
                     use_multiprocessing=train_var.use_multiprocessing)

        #************************************************************************************
        print('**********************************************************************') 
        print('delete the model')
        del my_model


#        if generate_pdf:     
#            generate_tex(output_path, experiment_name, NN_output.names, NN_output.log_label, author_of_the_experiment)     
#    sess.close()
    try:
        shutil.rmtree("./scalers")
    except: pass
    return