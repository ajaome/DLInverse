# -*- coding: utf-8 -*-
import numpy
import os
import tensorflow as tf
from tensorflow.python.keras import backend as K
from tensorflow.keras.callbacks import Callback
from DNN_TRAINING.save_load_model import save_weights_and_optimizer
from GENERATE_RESULTS.generate_results import save_load_history,save_error_header,save_error_line
from GENERATE_RESULTS.generate_results import evaluate_realative_and_absolute_errors_all_data_partitions
from GENERATE_RESULTS.generate_graphics import plot_errors, plot_new_error
from DNN_TRAINING.save_load_model import save_architecture_json
#import matplotlib.pyplot as plot


class show_basics(Callback):
    def on_train_begin(self, loop, logs={} ):
        print ("on_train_begin")
        return

    def on_epoch_begin(self, epoch, logs={}):
        print ("on_epoch_begin")
        return

    def on_epoch_end(self, epoch, logs={}):
        print ("on_epoch_end")       
        return

    def on_train_end(self, logs={}):
        print ("on_train_end")
        return

class save_weights_and_optimizer_state_each_n_epochs(Callback):
    def on_train_begin(self, logs):
        try:
            os.makedirs(self.model.experiment_folder + '/model_files/weights')
            os.makedirs(self.model.experiment_folder + '/model_files/optimizer_state')
        except:
            pass    
    def on_epoch_end(self, epoch, logs={}):
        if (epoch % int(self.model.save_each_n_epoch) == 0):
            path_w = self.model.experiment_folder + '/model_files/weights/' + self.model.experiment_name + "_weights_epoch_"+ str(epoch)+'.h5'
            path_op = self.model.experiment_folder + '/model_files/optimizer_state/' + self.model.experiment_name + "_optimizer_epoch_" + str(epoch)+'.pkl'
            save_weights_and_optimizer(self.model, path_w, path_op)
        return


class save_last_models(Callback):
    
    def on_train_begin(self, logs):    
        try:
            os.makedirs(self.model.experiment_folder +'/model_files/h5')
        except:
            pass
        return
    
    def on_train_end(self, logs):
        
        path =  self.model.experiment_folder + '/model_files/h5/'+self.model.experiment_name
        print ("***Saving model... " + self.model.experiment_name)
        self.model.save(path + '.h5')
        try:
            for submodel in self.model.submodels:
              print ("***Saving submodel... " + submodel.name)
              submodel.save( path+'_'+ submodel.name +'.h5')
        except:
            pass
        return


class save_architecture(Callback):
    def on_train_begin(self, logs):
        path = self.model.experiment_folder+'/model_files/json'
        filename = path +'/' + self.model.experiment_name + '_architecture.json'
        try:
            os.makedirs(path)
        except:
            pass
        save_architecture_json(self.model, filename)
        return
    

class save_history(Callback): 
    
    def __init__(self, loss_scale = "log", linthreshy=0.000000001, save_pdf=True, save_png= True, **kwargs):
        super(save_history, self).__init__(**kwargs)
        #TODO: put more options to configurate the graphics
        class plot_options():
            def __init__(self, loss_scale, save_pdf, save_png):
                self.loss_scale = loss_scale  #implemented for "log" "symlog" scales
                self.pdf = save_pdf
                self.png = save_png
                self.linthreshy= linthreshy #used with symlog
            
        self.graphic_configuration = plot_options(loss_scale, save_pdf, save_png)
             
    def on_train_begin(self, logs=None):
        self.epoch = []
        self.lrates = []
        self.history = {}
        try:
            os.makedirs(self.model.experiment_folder + '/history')
        except:
            pass
        return
    
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epoch.append(epoch)
        lr = float( K.get_value(self.model.optimizer.lr))
        self.lrates.append(lr)
        for k, v in logs.items():
          self.history.setdefault(k, []).append(v)
        return
    
    def on_train_end(self, logs={}):
        print("*****Saving history...")
        save_load_history(self.model.experiment_folder, self.model.experiment_name, self.epoch, self.lrates, self.history, self.graphic_configuration)
        return
    
    
class errors(Callback):
#TODO: we need select the norm-type in this callback
    #generate the files with header and previous epochs information
    def on_train_begin(self, logs=None):
        training_error_path=self.model.experiment_folder+'/train/'
        validation_error_path=self.model.experiment_folder+'/validation/'
        files = [training_error_path+"relative_error.dat", training_error_path+"absolute_error.dat", validation_error_path+"relative_error.dat", validation_error_path+"absolute_error.dat"]
        for fname in files:
            try:
              #load old data
              old_values = numpy.loadtxt(fname, delimiter="\t", skiprows=1)
              #write the new file
              save_error_header(fname, self.model.y_names[0])
              for line in old_values:
                  epoch=line[0]
                  save_error_line(fname, epoch, line[1:])
                  if (epoch == self.model.start_epoch):
                      break
            except: 
                save_error_header(fname, self.model.y_names[0])
        return
    #evaluate each error type for one epoch and append them in the files
    def on_epoch_end(self, epoch, logs=None):
        training_error_path=self.model.experiment_folder+'/train/'
        validation_error_path=self.model.experiment_folder+'/validation/'
        
        #compute errors
#TODO: we need select the norm-type here!!!
        absololute_error_training, relative_error_training, \
        absololute_error_validation, relative_error_validation=\
            evaluate_realative_and_absolute_errors_all_data_partitions(self.model,
                                                                       self.model.x_train[0], self.model.y_train[0],
                                                                       self.model.x_validation[0], self.model.y_validation[0])
        #append in the file
        save_error_line (training_error_path+ 'absolute_error.dat', epoch, absololute_error_training)
        save_error_line (training_error_path+ 'relative_error.dat', epoch, relative_error_training)
        save_error_line (validation_error_path+ 'absolute_error.dat', epoch, absololute_error_validation)
        save_error_line (validation_error_path+ 'relative_error.dat', epoch, relative_error_validation)
        return
    
    def on_train_end(self, logs={}):
        print("*****Saving error plots...")
        training_error_path=self.model.experiment_folder+'/train/'
        validation_error_path=self.model.experiment_folder+'/validation/'
        #
        plot_new_error(self.model.experiment_folder,training_error_path, validation_error_path,'absolute_error', self.model.y_names[0])
        plot_new_error(self.model.experiment_folder,training_error_path, validation_error_path,'relative_error', self.model.y_names[0])
        return
    

class Loss_Dependent_Adaptive_Learning_Rate(Callback):
#TODO: we need to save the best_losses_history
    def __init__(self,
                 printing_frequency = 0,
                 lr_down_factor = 0.9,
                 loss_down_threshold_factor = 0.95,
                 lr_up_factor = 1.01,
                 loss_memory = 30,
                 loss_improvement_proportion = 10**(-3),
                 best_losses_history = []):

        super(Loss_Dependent_Adaptive_Learning_Rate, self).__init__()

        self.printing_frequency = printing_frequency
        self.lr_down_factor = lr_down_factor
        self.loss_down_threshold_factor = loss_down_threshold_factor
        self.lr_up_factor = lr_up_factor
        self.best_losses_history = best_losses_history
        self.loss_memory = loss_memory
        self.loss_improvement_proportion = loss_improvement_proportion

    def on_epoch_begin(self, epoch, logs={}):

        if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0:
            print("\nEPOCH", epoch + 1)
        # In the first iteration
        if not epoch > 0:
            self.w_epoch = list()
            for layer in self.model.layers:
                self.w_epoch.append(layer.get_weights())

            self.w_best = self.w_epoch
            self.bool_previously_weights_accepted = True

    def on_epoch_end(self, epoch, logs={}):

        if self.model.stop_training:
            return

        self.loss_epoch = logs.get('loss')
        self.lr_epoch = K.get_value(self.model.optimizer.lr)

        self.w_epoch_plus_one = list()
        for layer in self.model.layers:
            self.w_epoch_plus_one.append(layer.get_weights())

        if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
            print("\tBEFORE")
            print("\tLoss Best:", self.loss_best)
            print("\tLoss {}:".format(epoch+1), self.loss_epoch)
            print("\tLr   {}:".format(epoch+1), self.lr_epoch)

        # In the first iteration
        if not epoch > 0:
            self.loss_best = self.loss_epoch

            if self.printing_frequency:
                print("\nEPOCH 1")
                print("\tLoss Best: ", self.loss_best)
                print("\tLoss 0: ", self.loss_epoch)
                print("\tLr   1: ", self.lr_epoch)

        else:
            # If the loss of the previous model is worse
            if self.loss_epoch > 0.9999 * self.loss_best and self.bool_previously_weights_accepted:

                # We decrease the learning rate
                K.set_value(self.model.optimizer.lr, self.lr_epoch * self.lr_down_factor)

                # We reject the weights
                self.w_epoch_plus_one = self.w_best
                for i in range(len(self.w_epoch_plus_one)):
                    self.model.layers[i].set_weights(self.w_epoch_plus_one[i])

                if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
                    print("\n\t*REJECTED WEIGHTS*")
                    print("\t*Decrease Learning rate*\n")

                self.bool_previously_weights_accepted = False
                self.loss_epoch = self.loss_best

            else:
                # We accept the weights
                if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
                    print("\n\t*ACCEPTED WEIGHTS*")

                self.w_best = self.w_epoch

                # If the convergence is slow
                if abs(self.loss_epoch) > self.loss_down_threshold_factor * abs(self.loss_best) and self.bool_previously_weights_accepted:
                    # We increase the learning rate
                    K.set_value(self.model.optimizer.lr, self.lr_epoch * self.lr_up_factor)

                    if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
                        print("\t*Increase Learning Rate*\n")

                else:
                    self.bool_previously_weights_accepted = True
                    if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
                        print("\t*Maintain Learning Rate*\n")

                self.loss_best = self.loss_epoch

        if self.printing_frequency and (epoch + 1) % self.printing_frequency == 0 and epoch > 0:
            print("\tAFTER")
            print("\tLoss Best:", self.loss_best)
            print("\tLoss {}:".format(epoch + 1), self.loss_epoch)
            print("\tLr   {}:".format(epoch + 2), K.get_value(self.model.optimizer.lr))

        self.w_epoch = self.w_epoch_plus_one

        self.best_losses_history.append(self.loss_best)

        ####### Early stopping depending on best losses #######

        try:
            if self.loss_best > (1-self.loss_improvement_proportion) * self.best_losses_history[-self.loss_memory]:
                self.model.stop_training = True
                print("\n\tSTOP TRAINING: loss improvement below a {} relative proportion after {} iterations".format(self.best_loss_improvement_proportion, self.loss_memory))

        except:
            pass

    def on_train_end(self, logs={}):

        # We update the model with the best weights whose loss is known
        for i in range(len(self.w_best)):
            self.model.layers[i].set_weights(self.w_best[i])
        # TODO: We save the file with the best weights here
        