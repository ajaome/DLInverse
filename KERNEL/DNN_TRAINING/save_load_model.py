# -*- coding: utf-8 -*-
import pickle
import os
import re
from tensorflow.keras.models import model_from_json
from tensorflow.keras.backend import batch_get_value


def save_weights_and_optimizer(model, model_weights_filename, model_optimizer_filename):
        model.save_weights(model_weights_filename)
        #save the state of the optimizer
        symbolic_weights = getattr(model.optimizer, 'weights')
        weight_values = batch_get_value(symbolic_weights)
        with open(model_optimizer_filename, 'wb') as f:
            pickle.dump(weight_values, f)    
        return
     
def save_architecture_json(model, filename):
        json_string = model.to_json()
        with open(filename, 'w') as outfile:
            outfile.write(json_string) 
        return

def load_architecture(arch_json_filename, my_custom_objects):
    with open(arch_json_filename,'r') as j_file:
                    model = model_from_json(j_file.read(),  custom_objects=my_custom_objects)
    return model

def load_weights_and_optimizer(model, experiment_name, model_folder, load_epoch):
    weights_filename = model_folder +'/model_files/weights/'+experiment_name +'_weights_epoch_' +str(load_epoch)+'.h5' 
    model.load_weights(weights_filename)
    model._make_train_function()
    model_optimizer_filename = model_folder +'/model_files/optimizer_state/'+experiment_name + '_optimizer_epoch_' +str(load_epoch)+'.pkl'
    with open(model_optimizer_filename, 'rb') as f:
        weight_values = pickle.load(f)
    model.optimizer.set_weights(weight_values)
    return model


def remove_old_files(folder,experiment_name,epoch_loaded):
    model_folder= folder+ '/model_files/weights/'
    for file in os.listdir(model_folder):
        string=file.replace('.h5','')
        number=int(re.findall('\d+',string.replace(experiment_name + '_weights_epoch_',''))[0])
        if (number > epoch_loaded):
           print(model_folder+file)
           os.remove(model_folder+file)
           print('deleted')  
    model_folder= folder+ '/model_files/optimizer_state/'
    for file in os.listdir(model_folder):
        string=file.replace('.pkl','')
        number=int(re.findall('\d+',string.replace(experiment_name + '_optimizer_epoch_',''))[0])
        if (number > epoch_loaded):
           print(model_folder+file)
           os.remove(model_folder+file)
           print('deleted')  
    return

def remove_all_files(folder):
    for file in os.listdir(folder):
        os.remove(folder+'/'+file)
    return   
        
def search_last_epoch(folder,experiment_name):
    last_epoch=0
    for file in os.listdir(folder + '/model_files/weights/'):
        string=file.replace('.h5','')
        number=int(re.findall('\d+',string.replace(experiment_name,''))[0])
        if (number > last_epoch):
            last_epoch = number
    return last_epoch