# -*- coding: utf-8 -*-
#import tensorflow as tf
from tensorflow.keras import optimizers
#import tensorflow.keras.backend as K
#from tensorflow.keras.callbacks import Callback, ModelCheckpoint, TensorBoard, ReduceLROnPlateau
#import pickle
#import os.path
#import numpy
#import json
#import matplotlib.pyplot as plot
#from LOSS_FUNCTIONS.regularizations import gradient_regularization_l1_normalized

class training_variables():
    optimizers = None
    random_seed = 123
    number_samples = None
    batch_size = None
    number_epochs = None
    retrain_from_epoch = None
    verbose = 1
    callbacks = []
    validation_split = 0.0
    shuffle = True
    class_weight = None
    sample_weight = None
    steps_per_epoch = None
    validation_steps = None
    validation_freq = 1
    max_queue_size = 10
    workers = 1
    use_multiprocessing = False
    save_each_n_epoch = 1       
    
class model_variables():
    name      = ['default']
    function  = []
    optimizer = ['rmsprop']
    norm_type = ["l1"]
    loss = ['mae']
    loss_weights = [None]
    metrics =[['mse', 'mae'] ]

def default_optimizer_list():
    # initialize the optimizers
    optimizers_lst = {}
    optimizers_lst['rmsprop'] = optimizers.RMSprop(lr=0.0001, decay=1e-6)
    optimizers_lst['rmsprop2'] = optimizers.RMSprop(lr=0.000001, decay=1e-14)
    optimizers_lst['ada_grad'] = optimizers.Adagrad()
    optimizers_lst['ada_delta'] = optimizers.Adadelta()
    optimizers_lst['adam'] = optimizers.Adam()
    optimizers_lst['nadam'] = optimizers.Nadam()
    optimizers_lst['nadam2'] = optimizers.Nadam(lr=0.00001)
    optimizers_lst['adamax'] = optimizers.Adamax()
    optimizers_lst['sgd'] = optimizers.SGD(lr=10.E-3, decay=10.0E-8, momentum=0.9, nesterov=True)
    optimizers_lst['sgd2'] = optimizers.SGD(lr=10.E-8, decay=10.0E-14, momentum=0.9, nesterov=True)
    return optimizers_lst

