# -*- coding: utf-8 -*-
from tensorflow.keras.utils import plot_model
from tensorflow.keras.layers import concatenate, Reshape, Dense, Input, Flatten, Add, Conv1D, UpSampling1D, MaxPooling1D
from tensorflow.keras.initializers import glorot_normal
from tensorflow.keras.models import load_model, Model
from LOSS_FUNCTIONS.regularizations import gradient_regularization_l1_normalized, gradient_y, gradient_regularization_l1_normalized
from NORMS.norms import abs_error_l1_norm_normalized
from DNN_ARCHITECTURES.dual_topology import BilinearUpsampling1D

def generate_model_I(input_shape,outputs_shape, pool_length=2, nb_filter=40,num_blocks=5, blocks_per_maxpooling=1):

    input_ = Input(shape=input_shape)
    x = input_
    for i in range(num_blocks):
        a = Conv1D(filters=nb_filter * (i + 1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same', name='block_conv_A_%d' % i)(x)
        d = Conv1D(filters=nb_filter * (i + 1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same', name='block_conv_B_%d' % i)(a)
        x = Add()([a, d])
        # x = Dropout(0.25)(x)
        if (i + 1) % blocks_per_maxpooling == 0:
            x = MaxPooling1D(pool_length)(x)

    x = Flatten()(x)
    y = Dense(outputs_shape, activation='relu', kernel_initializer='glorot_uniform',name='classification_layer')(x)
    model = Model(input_,y)
    model.summary()
    return model
def generate_model_F(input_shape, output_shape, pool_length=2, nb_filter=40, num_blocks=5, blocks_per_maxpooling=1):
    input_ = Input(input_shape)
    x= input_
    x = Reshape((1,input_shape[0]))(x)
    for i in range(num_blocks):
        a = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_A_%d' %i)(x)
        d = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_B_%d' %i)(a)
        x = Add()([a, d])
        if (i + 1) % blocks_per_maxpooling == 0:
            x = UpSampling1D(pool_length)(x)
    #Last layer
    x = BilinearUpsampling1D(output_shape[0])(x)
    decoded = Conv1D(filters=output_shape[-1], kernel_size=1, activation='linear',
                     kernel_initializer=glorot_normal(), padding='same', name='final_deconv')(x)

    model = Model(input_, decoded)
#    model.summary()
    return model



def generate_model_I_FI_F_IF(shape_measurements,shape_trajectory_for_measurements,shape_materials,
                             shape_trajectory_for_materials,pool_length=2, nb_filter=40,num_blocks=5,
                             F_trained=False, F_trained_model='TRAINED_MODELS/materials2measurements.h5'):

    # initialize input shapes and input layers for measurements
    input_layer_measurements = Input(shape=(shape_measurements[0], shape_measurements[1]))
    input_layer_trajectory_for_measurements = Input(shape=(shape_trajectory_for_measurements[0], shape_trajectory_for_measurements[1]))
    input_shape_measurements_and_trajectory = (shape_measurements[0], shape_measurements[1]+shape_trajectory_for_measurements[1])
    input_layer_inverse = concatenate([input_layer_measurements, input_layer_trajectory_for_measurements])

    # initialize input shapes and input layers for materials
    input_layer_materials = Input(shape=(shape_materials,))
    input_layer_trajectory_for_materials = Input(shape=(shape_trajectory_for_materials,))
    input_shape_materials_and_trajectory = (shape_materials+shape_trajectory_for_materials,)
    input_layer_forward = concatenate([input_layer_materials, input_layer_trajectory_for_materials])

    # build measurements to materials DNN
    measuremets2materials = generate_model_I(input_shape_measurements_and_trajectory, shape_materials, pool_length, nb_filter, num_blocks)

    # build materials to measurements DNN
    if F_trained:
        materials2measurements = load_model(F_trained_model,
                                            custom_objects={'gradient_regularization_l1_normalized': gradient_regularization_l1_normalized, 'gradient_y': gradient_y,
                                                            'abs_error_l1_norm_normalized': abs_error_l1_norm_normalized,
                                                            'gradient_regularization_l1_normalized': gradient_regularization_l1_normalized,
                                                            'BilinearUpsampling1D': BilinearUpsampling1D})
        for layer in materials2measurements.layers:
            layer.trainable = False

        materials2measurements.summary()
    else:
        materials2measurements = generate_model_F(input_shape_materials_and_trajectory, shape_measurements, pool_length, nb_filter, num_blocks)

    # I_MODEL
    i = measuremets2materials(input_layer_inverse)

    # FI_MODEL
    f_i = materials2measurements(concatenate([measuremets2materials(input_layer_inverse),input_layer_trajectory_for_materials]))

    # F_MODEL
    f = materials2measurements(input_layer_forward)

    # IF_MODEL
    i_f = measuremets2materials(concatenate([materials2measurements(input_layer_forward),input_layer_trajectory_for_measurements]))

    # wrap the whole model
    model = Model(inputs=[input_layer_measurements, input_layer_trajectory_for_measurements, input_layer_materials, input_layer_trajectory_for_materials], outputs=[i, f_i, f, i_f])
    model.summary()
    return model, measuremets2materials, materials2measurements


def generate_model_I_F_FI(shape_measurements,shape_trajectory_for_measurements,
                          shape_materials,shape_trajectory_for_materials,pool_length=2,
                          nb_filter=40,num_blocks = 5, F_trained=False, F_trained_model='TRAINED_MODELS/materials2measurements.h5'):

    # initialize input shapes and input layers for measurements
    input_layer_measurements = Input(shape=(shape_measurements[0], shape_measurements[1]))
    input_layer_trajectory_for_measurements = Input(shape=(shape_trajectory_for_measurements[0], shape_trajectory_for_measurements[1]))
    input_shape_measurements_and_trajectory = (shape_measurements[0], shape_measurements[1]+shape_trajectory_for_measurements[1])
    input_layer_inverse = concatenate([input_layer_measurements, input_layer_trajectory_for_measurements])

    # initialize input shapes and input layers for materials
    input_layer_materials = Input(shape=(shape_materials,))
    input_layer_trajectory_for_materials = Input(shape=(shape_trajectory_for_materials,))
    input_shape_materials_and_trajectory = (shape_materials+shape_trajectory_for_materials,)
    input_layer_forward = concatenate([input_layer_materials, input_layer_trajectory_for_materials])

    # build measurements to materials DNN
    measuremets2materials = generate_model_I(input_shape_measurements_and_trajectory, shape_materials, pool_length, nb_filter, num_blocks)

    # build materials to measurements DNN
    if F_trained:
        materials2measurements = load_model(F_trained_model,
                                            custom_objects={'gradient_regularization_l1_normalized': gradient_regularization_l1_normalized, 'gradient_y': gradient_y,
                                                            'abs_error_l1_norm_normalized': abs_error_l1_norm_normalized,
                                                            'gradient_regularization_l1_normalized': gradient_regularization_l1_normalized,
                                                            'BilinearUpsampling1D':BilinearUpsampling1D})
        for layer in materials2measurements.layers:
            layer.trainable = False

        #materials2measurements.summary()

    else:
        materials2measurements = generate_model_F(input_shape_materials_and_trajectory, shape_measurements, pool_length, nb_filter, num_blocks)

    # I_MODEL
    i = measuremets2materials(input_layer_inverse)

    # FI_MODEL
    f_i = materials2measurements(concatenate([measuremets2materials(input_layer_inverse),input_layer_trajectory_for_materials]))

    # F_MODEL
    f = materials2measurements(input_layer_forward)

    # wrap the whole model
    model = Model(inputs=[input_layer_measurements, input_layer_trajectory_for_measurements, input_layer_materials, input_layer_trajectory_for_materials], outputs=[i, f_i, f])
    model.summary()
    return model, measuremets2materials, materials2measurements





#if __name__ == "__main__":
#
#    model = generate_model_I_FI_F_FI((65,6), (65,2), 7, 2, pool_length=2, nb_filter=40, num_blocks=5, F_trained=True)
    # task2_loss_weight = 1.0
    # sgd = optimizers.SGD(lr=0.001, decay=0.000001, momentum=0.9, nesterov=True)
    # metrics = ['mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh','mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh']
    # model.compile(optimizer=sgd,
    #                       loss=[gradient_regularization_l1_normalized,losses.mean_absolute_error,losses.mean_absolute_error,gradient_regularization_l1_normalized],
    #                       loss_weights=[1.,1. ,1. ,1.],
    #                       metrics=metrics)