# -*- coding: utf-8 -*-
# !/usr/bin/python
from numpy import random
random.seed(1)
from tensorflow.compat.v1 import set_random_seed
set_random_seed(2)
#import tensorflow.keras
from tensorflow.keras import optimizers
from tensorflow.keras.layers import GaussianNoise, Add, Dense, Activation, Input, Flatten
from tensorflow.keras.layers import LSTM, MaxPooling1D, Dropout, Conv1D, Reshape, GRU
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.initializers import glorot_normal
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.regularizers import l2


def create_model(input_shape, num_outputs,use_adam,sigma_input=0.02, w_maxnorm=2, drop1=0.25, drop2=0.5, l_conv=31, m_conv=3, l_pool=2, l_gru=10):
    i = Input(shape=input_shape)
    x = GaussianNoise(sigma_input)(i)
    x = Conv1D(l_conv, m_conv, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = MaxPooling1D(l_pool)(x)
    x = Dropout(drop1)(x)
    x = Conv1D(l_conv, m_conv, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = MaxPooling1D(l_pool)(x)
    x = Dropout(drop1)(x)


    x = GRU(l_gru, kernel_constraint=max_norm(w_maxnorm), kernel_initializer=glorot_normal(seed=1))(x)
    # x2 = GRU(l_gru, kernel_constraint=max_norm(w_maxnorm), go_backwards=True, kernel_initializer=glorot_normal(seed=1))(
    #     x)
    # x = concatenate([x1, x2])
    x = Dropout(drop2)(x)

    o = Dense(num_outputs, activation='sigmoid')(x)

    model = Model(inputs=i, outputs=o)

    if use_adam:
        model.compile(loss='logcosh',
                  optimizer='rmsprop',
                  # optimizer='adam',
                  metrics=['mean_squared_error', 'mse', 'mae', 'mape'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='logcosh',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape'])
    model.summary()
    return model


def create_model_topology_1D2_(input_shape, num_outputs, pool_length=2, nb_filter=40, filter_length=13,
                               recurrent_output_size=60, use_adam=False, use_lstm=True,sigma_input=0.02):
    model = Sequential()
    #

    model.add(Conv1D(activation="relu", input_shape=input_shape, filters=nb_filter, kernel_size=filter_length,
                     padding="valid", kernel_initializer='glorot_uniform'))
    model.add(
        Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length // pool_length + 1, padding="valid",
               kernel_initializer='glorot_uniform'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Dropout(0.25))

    model.add(Conv1D(activation="relu", input_shape=input_shape, filters=nb_filter, kernel_size=filter_length,
                     padding="valid", kernel_initializer='glorot_uniform'))
    model.add(
        Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length // pool_length + 1, padding="valid",
               kernel_initializer='glorot_uniform'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Dropout(0.25))

    if use_lstm:
        model.add(LSTM(recurrent_output_size,
                       input_shape=(model.layers[-1].output_shape[1], model.layers[-1].output_shape[2]),
                       activation='relu'))
    else:
        model.add(Flatten(input_shape=input_shape))
    # model.add(Dense(60, activation='relu',kernel_initializer='normal'))
    # model.add(Dense(60, activation='relu',kernel_initializer='normal'))
    model.add(Dense(512, activation='relu', kernel_initializer='glorot_uniform'))
    # model.add(Dense(num_outputs, activation='tanh'))
    model.add(Dense(num_outputs, activation='linear', kernel_initializer='glorot_uniform'))

    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='logcosh', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='logcosh',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model


def create_model_topology_blocked(input_shape, num_outputs, pool_length=2, nb_filter=40,
                               recurrent_output_size=60, use_adam=False, use_lstm=True,w_maxnorm=2.0):
    i = Input(shape=input_shape)

    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(i)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)


    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)


    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_constraint=max_norm(w_maxnorm),
               kernel_initializer=glorot_normal(seed=1))(x)
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)



    if use_lstm:
        x= LSTM(recurrent_output_size,
                       activation='relu')(x)
    else:
        x= Flatten(input_shape=input_shape)(x)


    x = Dense(512, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='logcosh', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='logcosh',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape'])
    model.summary()
    return model


def create_model_resnet1D(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=60, use_adam=False, use_lstm=True, w_maxnorm=5.0):
    i = Input(shape=input_shape)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(i)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)


    if use_lstm:
        x= LSTM(recurrent_output_size,
                       activation='relu')(x)
    else:
        x= Flatten(input_shape=input_shape)(x)


    # x = Dense(128, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model


def create_model_lstm_resnet1D(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=60, use_adam=False, w_maxnorm=5.0):
    i = Input(shape=input_shape)
    x = LSTM(recurrent_output_size)(i)
    x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x2)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)
    #
    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)



    # if use_lstm:
    #     x= LSTM(recurrent_output_size,
    #                    activation='relu')(x)
    # else:
    x= Flatten(input_shape=input_shape)(x)


    # x = Dense(128, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    # opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    # if use_adam:
    #     model.compile(loss='mse', optimizer=opt,
    #                   metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    # else:
    #     sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
    #     model.compile(optimizer=sgd,
    #                   loss='mse',
    #                   metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    # model.summary()
    # return model

    # rmsprop = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    # ada_grad = optimizers.Adagrad()
    # ada_delta = optimizers.Adadelta()
    # nadam = optimizers.Nadam()
    # adamax = optimizers.Adamax()
    # # tf = optimizers.TFOptimizer()
    # sgd = optimizers.SGD(lr=10.E-3, decay=10.0E-8, momentum=0.9, nesterov=True)
    #
    # if use_adam:
    #     model.compile(loss='mse', optimizer=rmsprop,
    #                   metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh'])
    # else:
    #     model.compile(optimizer=sgd,
    #                   loss='mse',
    #                   metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh'])
    # model.summary()
    return model



def create_model_resnet1D_skip(input_shape, num_outputs, pool_length=2, nb_filter=250,
                          recurrent_output_size=60, use_adam=False, w_maxnorm=5.0,sigma_input=0.02):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=True, b_max_pooling=True)
    x = residual_block_1D(x, nb_filter=nb_filter * 3, b_drop_out=True, b_max_pooling=True)
    x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=True, b_max_pooling=True)
    x = residual_block_1D(x, nb_filter=nb_filter * 5, b_drop_out=True, b_max_pooling=True)
    x = residual_block_1D(x, nb_filter=nb_filter * 6, b_drop_out=True, b_max_pooling=True)
    x= Flatten(input_shape=input_shape)(x)
    x = Dense(2048, activation='relu', kernel_initializer='glorot_uniform')(x)
    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model


def create_model_lstm_resnet1D_skip(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=60, use_adam=False, w_maxnorm=5.0,sigma_input=0.05):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = LSTM(recurrent_output_size)(x)
    x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    x = residual_block_1D(x2, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x2)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([x2,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([x,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([x,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)
    #
    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)



    # if use_lstm:
    #     x= LSTM(recurrent_output_size,
    #                    activation='relu')(x)
    # else:
    x= Flatten(input_shape=input_shape)(x)


    # x = Dense(128, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model

def create_model_resnet1D_skip_lstm(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=60, use_adam=False, w_maxnorm=5.0,sigma_input=0.05):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)


    # x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([x,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([x,d])
    # x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)
    #
    # a = Conv1D(filters=nb_filter, kernel_size=15, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(x)
    # b = Conv1D(filters=nb_filter, kernel_size=7, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(a)
    # c = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(b)
    # d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
    #            kernel_initializer=glorot_normal(seed=1),padding='same')(c)
    # x = Concatenate()([a,b,c,d])
    # x = Dropout(0.25)(x)
    # x = MaxPooling1D(pool_length)(x)



    # if use_lstm:
    #
    # else:
    # x= Flatten(input_shape=input_shape)(x)
    x = LSTM(recurrent_output_size,activation='relu')(x)

    # x = Dense(128, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model




def create_model_lstm_resnet1D_v2(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=120, use_adam=False, w_maxnorm=5.0):
    i = Input(shape=input_shape)
    x = LSTM(recurrent_output_size,activation='relu',recurrent_activation='relu',)(i)
    x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x2)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    a = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(x)
    d = Conv1D(filters=nb_filter, kernel_size=3, activation='relu',
               kernel_initializer=glorot_normal(),padding='same')(a)
    x = Add()([a,d])
    x = Dropout(0.25)(x)
    x = MaxPooling1D(pool_length)(x)

    x= Flatten(input_shape=input_shape)(x)


    # x = Dense(128, activation='relu', kernel_initializer='glorot_uniform')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='logcosh', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='logcosh',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model

def create_model_topology_1D(input_shape, num_outputs, pool_length=2, nb_filter=20, filter_length=13,
                             recurrent_output_size=60, use_adam=False, use_lstm=True,sigma_input=0.02):
    # return create_model_topology_1D2_(input_shape, num_outputs, pool_length, nb_filter , filter_length ,recurrent_output_size ,use_adam ,use_lstm)
    model = Sequential()
    #

    model.add(Conv1D(activation="relu", input_shape=input_shape, filters=nb_filter, kernel_size=filter_length,
                     padding="valid", kernel_initializer='glorot_uniform'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(
        Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length // pool_length + 1, padding="valid",
               kernel_initializer='glorot_uniform'))
    model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Dropout(0.25))
    # model.add(BatchNormalization())
    model.add(
        Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length // pool_length + 1, padding="valid"))
    # model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length // pool_length // pool_length + 1,
                     padding="valid"))
    # model.add(MaxPooling1D(pool_size=pool_length))
    # model.add(Conv1D(activation="relu", filters=nb_filter, kernel_size=filter_length//pool_length//pool_length+1, padding="valid",kernel_initializer='normal'))
    # model.add(MaxPooling1D(pool_size=pool_length))
    model.add(Dropout(0.25))
    # model.add(BatchNormalization())
    #

    # model.add(Reshape((model.layers[-1].output_shape[2],model.layers[-1].output_shape[3])))

    if use_lstm:
        model.add(LSTM(recurrent_output_size,
                       input_shape=(model.layers[-1].output_shape[1], model.layers[-1].output_shape[2]),
                       activation='relu'))
    else:
        model.add(Flatten(input_shape=input_shape))
    # model.add(Dense(60, activation='relu',kernel_initializer='normal'))
    # model.add(Dense(60, activation='relu',kernel_initializer='normal'))
    model.add(Dense(60, activation='relu', kernel_initializer='glorot_uniform'))
    # model.add(Dense(num_outputs, activation='tanh'))
    model.add(Dense(num_outputs, activation='linear'))
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='logcosh', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='logcosh',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape'])
    model.summary()
    return model



def resnet_layer(inputs,
                 num_filters=16,
                 kernel_size=3,
                 strides=1,
                 activation='relu',
                 batch_normalization=True,
                 conv_first=True):
    """2D Convolution-Batch Normalization-Activation stack builder
    # Arguments
        inputs (tensor): input tensor from input image or previous layer
        num_filters (int): Conv2D number of filters
        kernel_size (int): Conv2D square kernel dimensions
        strides (int): Conv2D square stride dimensions
        activation (string): activation name
        batch_normalization (bool): whether to include batch normalization
        conv_first (bool): conv-bn-activation (True) or
            bn-activation-conv (False)
    # Returns
        x (tensor): tensor as input to the next layer
    """
    conv = Conv1D(num_filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  padding='same',
                  kernel_initializer='he_normal',
                  kernel_regularizer=l2(1e-4))

    x = inputs
    if conv_first:
        x = conv(x)
        # if batch_normalization:
        #     x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
    else:
        # if batch_normalization:
        #     x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
        x = conv(x)
    return x


def resnet_v2_1D(input_shape, depth, num_classes=10,use_adam =True):
    """ResNet Version 2 Model builder [b]
    Stacks of (1 x 1)-(3 x 3)-(1 x 1) BN-ReLU-Conv2D or also known as
    bottleneck layer
    First shortcut connection per layer is 1 x 1 Conv2D.
    Second and onwards shortcut connection is identity.
    At the beginning of each stage, the feature map size is halved (downsampled)
    by a convolutional layer with strides=2, while the number of filter maps is
    doubled. Within each stage, the layers have the same number filters and the
    same filter map sizes.
    Features maps sizes:
    conv1  : 32x32,  16
    stage 0: 32x32,  64
    stage 1: 16x16, 128
    stage 2:  8x8,  256
    # Arguments
        input_shape (tensor): shape of input image tensor
        depth (int): number of core convolutional layers
        num_classes (int): number of classes (CIFAR10 has 10)
    # Returns
        model (Model): Keras model instance
    """
    if (depth - 2) % 9 != 0:
        raise ValueError('depth should be 9n+2 (eg 56 or 110 in [b])')
    # Start model definition.
    num_filters_in = 16
    num_res_blocks = int((depth - 2) / 9)

    inputs = Input(shape=input_shape)
    # v2 performs Conv2D with BN-ReLU on input before splitting into 2 paths
    x = resnet_layer(inputs=inputs,
                     num_filters=num_filters_in,
                     conv_first=True)

    # Instantiate the stack of residual units
    for stage in range(3):
        for res_block in range(num_res_blocks):
            activation = 'relu'
            batch_normalization = True
            strides = 1
            if stage == 0:
                num_filters_out = num_filters_in * 4
                if res_block == 0:  # first layer and first stage
                    activation = None
                    batch_normalization = False
            else:
                num_filters_out = num_filters_in * 2
                if res_block == 0:  # first layer but not first stage
                    strides = 2    # downsample

            # bottleneck residual unit
            y = resnet_layer(inputs=x,
                             num_filters=num_filters_in,
                             kernel_size=1,
                             strides=strides,
                             activation=activation,
                             batch_normalization=batch_normalization,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_in,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_out,
                             kernel_size=1,
                             conv_first=False)
            if res_block == 0:
                # linear projection residual shortcut connection to match
                # changed dims
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters_out,
                                 kernel_size=1,
                                 strides=strides,
                                 activation='relu',
                                 batch_normalization=False)
            x = keras.layers.add([x, y])

        num_filters_in = num_filters_out

    # Add classifier on top.
    # v2 has BN-ReLU before Pooling
    # x = BatchNormalization()(x)
    x = Activation('relu')(x)
    # x = AveragePooling1D(pool_size=8)(x)
    y = Flatten()(x)
    outputs = Dense(num_classes,
                    activation='sigmoid',
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    model = Model(inputs=inputs, outputs=outputs)

    # initiate RMSprop optimizer
    # opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

    if use_adam:
        model.compile(loss='mse', optimizer='rmsprop',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-2, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])

    # Let's train the model using RMSprop
    # model.compile(loss='categorical_crossentropy',
    #               optimizer=opt,
    #               metrics=['accuracy'])
    model.summary()
    return model


# def residual_block_1D(x, nb_filter, b_drop_out, b_max_pooling, mp_length=2, drop_out_val=0.25, kernel_size=3,
#                       w_maxnorm=None):
#     if w_maxnorm is not None:
#         kernel_constraint = max_norm(w_maxnorm)
#     else:
#         kernel_constraint = None
#
#     short_cut = Conv1D(filters=nb_filter, kernel_size=1, activation='relu', kernel_initializer=glorot_normal(),
#                        padding='same', kernel_constraint=kernel_constraint)(x)
#     a = Conv1D(filters=nb_filter, kernel_size=kernel_size, activation='relu',
#                kernel_initializer=glorot_normal(), padding='same', kernel_constraint=kernel_constraint)(x)
#     d = Conv1D(filters=nb_filter, kernel_size=kernel_size, activation='relu',
#                kernel_initializer=glorot_normal(), padding='same', kernel_constraint=kernel_constraint)(a)
#     x = Add()([short_cut, d])
#     if b_drop_out:
#         x = Dropout(drop_out_val)(x)
#     if b_max_pooling:
#         x = MaxPooling1D(mp_length)(x)
#
#     return x

def residual_block_1D(x, nb_filter, b_drop_out, b_max_pooling, mp_length=2, drop_out_val=0.25, kernel_size=3,
                      w_maxnorm=None):
    if w_maxnorm is not None:
        kernel_constraint = max_norm(w_maxnorm)
    else:
        kernel_constraint = None

    short_cut = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
                       padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(x)
    a = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(x)
    d = Conv1D(filters=nb_filter, kernel_size=kernel_size, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(a)
    d = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(d)
    x = Add()([short_cut, d])
    if b_drop_out:
        x = Dropout(drop_out_val)(x)
    if b_max_pooling:
        x = MaxPooling1D(mp_length)(x)

    return x

def create_model_resnet1D_progressive(input_shape, num_outputs, pool_length=2, nb_filter=64,use_adam=False, w_maxnorm=None,sigma_input=0.001):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = Conv1D(filters=64, kernel_size=7, activation='relu', padding='same', kernel_initializer=glorot_normal())(x)
    x = MaxPooling1D(pool_length)(x)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length, w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length, w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length, w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True, mp_length=pool_length, w_maxnorm=w_maxnorm)

    x = residual_block_1D(x, nb_filter=nb_filter*2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter*2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter*2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter*2, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,w_maxnorm=w_maxnorm)
    #
    # x = residual_block_1D(x, nb_filter=nb_filter*4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter*4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter*4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter*4, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,w_maxnorm=w_maxnorm)


    x= Flatten(input_shape=input_shape)(x)
    x = Dense(512, activation='relu', kernel_initializer='glorot_uniform')(x)
    y =  Dense(num_outputs, activation='linear', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-4, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model


def create_model_lstm_resnet1D_progressive(input_shape, num_outputs, pool_length=2, nb_filter=32,
                          recurrent_output_size=512, use_adam=False, w_maxnorm=None,sigma_input=0.002):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = LSTM(recurrent_output_size)(x)
    x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    x = Conv1D(filters=64, kernel_size=3, activation='relu', kernel_initializer=glorot_normal(), padding='same')(x2)
    x = Conv1D(filters=64, kernel_size=3, activation='relu', kernel_initializer=glorot_normal(), padding='same')(x)
    x = MaxPooling1D(pool_length)(x)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x= Flatten(input_shape=input_shape)(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-4, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model



def create_model_lstm_resnet1D_progressive_b(input_shape, num_outputs, pool_length=2, nb_filter=80,
                          recurrent_output_size=128, use_adam=False, w_maxnorm=None,sigma_input=0.002):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = LSTM(recurrent_output_size)(x)
    x2 = Reshape((recurrent_output_size,1))(x)
    # x3 = Permute((1, 2))(x2)

    x = Conv1D(filters=128, kernel_size=3, activation='relu', kernel_initializer=glorot_normal(), padding='same')(x2)
    x = Conv1D(filters=128, kernel_size=3, activation='relu', kernel_initializer=glorot_normal(), padding='same')(x)
    x = MaxPooling1D(pool_length)(x)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    # x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
    #                       w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
    #                       w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
    #                       w_maxnorm=w_maxnorm)
    # x = residual_block_1D(x, nb_filter=nb_filter * 4, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
    #                       w_maxnorm=w_maxnorm)

    x= Flatten(input_shape=input_shape)(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        ada_grad = optimizers.Adagrad()
        ada_delta = optimizers.Adadelta()
        nadam = optimizers.Nadam()
        adamax = optimizers.Adamax()
        # tf = optimizers.TFOptimizer()
        sgd = optimizers.SGD(lr=10.E-3, decay=10.0E-8, momentum=0.9, nesterov=True)
        model.compile(optimizer=nadam,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model



def create_model_resnet1D_lstm_progressive(input_shape, num_outputs, pool_length=2, nb_filter=40,
                          recurrent_output_size=512, use_adam=False, w_maxnorm=5.0,sigma_input=0.001):
    i = Input(shape=input_shape)

    x = GaussianNoise(sigma_input)(i)

    x = Conv1D(filters=64, kernel_size=3, activation='relu', kernel_initializer=glorot_normal(), padding='same')(x)
    x = MaxPooling1D(pool_length)(x)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=False, b_max_pooling=False, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)
    x = residual_block_1D(x, nb_filter=nb_filter * 2, b_drop_out=True, b_max_pooling=True, mp_length=pool_length,
                          w_maxnorm=w_maxnorm)

    x = LSTM(recurrent_output_size,activation='relu')(x)

    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform')(x)

    model = Model(i,y)
    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    if use_adam:
        model.compile(loss='mse', optimizer=opt,
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    else:
        sgd = optimizers.SGD(lr=10.E-4, decay=10.E-6, momentum=0.9, nesterov=True)
        model.compile(optimizer=sgd,
                      loss='mse',
                      metrics=['mean_squared_logarithmic_error', 'mse', 'mae', 'mape','logcosh'])
    model.summary()
    return model


if __name__ == "__main__":
    pass