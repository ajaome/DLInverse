# -*- coding: utf-8 -*-
import matplotlib as mpl
mpl.use('Agg')

from numpy import random
random.seed(1)
from tensorflow.compat.v1 import set_random_seed
set_random_seed(2)

import tensorflow.keras.backend as K
import tensorflow as tf
from tensorflow.keras import optimizers, losses
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.initializers import glorot_normal
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import Add, UpSampling1D, MaxPooling1D, Dropout, Conv1D, Dense, Input, Flatten
from tensorflow.keras.models import Model
from LOSS_FUNCTIONS.regularizations import gradient_regularization_l1_normalized

def residual_block_1D(x, nb_filter, b_drop_out, b_max_pooling, mp_length=2, drop_out_val=0.25, kernel_size=3,
                      w_maxnorm=None):
    if w_maxnorm is not None:
        kernel_constraint = max_norm(w_maxnorm)
    else:
        kernel_constraint = None

    short_cut = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
                       padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(x)
    a = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(x)
    d = Conv1D(filters=nb_filter, kernel_size=kernel_size, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(a)
    d = Conv1D(filters=nb_filter, kernel_size=1, activation='relu',
               padding='same', kernel_constraint=kernel_constraint ,kernel_initializer=glorot_normal())(d)
    x = Add()([short_cut, d])
    if b_drop_out:
        x = Dropout(drop_out_val)(x)
    if b_max_pooling:
        x = MaxPooling1D(mp_length)(x)

    return x



#
# class BilinearUpsampling(Layer):
#     """Just a simple bilinear upsampling layer. Works only with TF.
#        Args:
#            upsampling: tuple of 2 numbers > 0. The upsampling ratio for h and w
#            output_size: used instead of upsampling arg if passed!
#     """
#
#     def __init__(self, upsampling=(2, 2), output_size=None, data_format=None, **kwargs):
#
#         super(BilinearUpsampling, self).__init__(**kwargs)
#
#         self.data_format = K.common.normalize_data_format(data_format)
#         self.input_spec = InputSpec(ndim=4)
#         if output_size:
#             self.output_size = conv_utils.normalize_tuple(
#                 output_size, 2, 'output_size')
#             self.upsampling = None
#         else:
#             self.output_size = None
#             self.upsampling = conv_utils.normalize_tuple(
#                 upsampling, 2, 'upsampling')
#
#
#     def compute_output_shape(self, input_shape):
#         if self.upsampling:
#             height = self.upsampling[0] * \
#                 input_shape[1] if input_shape[1] is not None else None
#             width = self.upsampling[1] * \
#                 input_shape[2] if input_shape[2] is not None else None
#         else:
#             height = self.output_size[0]
#             width = self.output_size[1]
#         return (input_shape[0],
#                 height,
#                 width,
#                 input_shape[3])
#
#     def call(self, inputs):
#         if self.upsampling:
#             return K.tf.image.resize_bilinear(inputs, (inputs.shape[1] * self.upsampling[0],
#                                                        inputs.shape[2] * self.upsampling[1]),
#                                               align_corners=True)
#         else:
#             return K.tf.image.resize_bilinear(inputs, (self.output_size[0],
#                                                        self.output_size[1]),
#                                               align_corners=True)
#
#     def get_config(self):
#         config = {'upsampling': self.upsampling,
#                   'output_size': self.output_size,
#                   'data_format': self.data_format}
#         base_config = super(BilinearUpsampling, self).get_config()
#         return dict(list(base_config.items()) + list(config.items()))
#


class BilinearUpsampling1D(Layer):
    """
    Wrapping 1D BilinearUpsamling as a Keras layer
    Input: 3D Tensor (batch, dim, channels)
    """
    def __init__(self, size=65, **kwargs):
        self.size = size
        super(BilinearUpsampling1D, self).__init__(**kwargs)

    def build(self, input_shape):
        super(BilinearUpsampling1D,self).build(input_shape)

    def call(self, x, mask=None):
        x = K.expand_dims(x, axis=2)
        x = tf.image.resize(x, [self.size, 1])
        x = K.squeeze(x, axis=2)
        return x

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.size, input_shape[2])

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.size, input_shape[2])
    
    def get_config(self):
         config = {'size': self.size}
         base_config = super(BilinearUpsampling1D, self).get_config()
         return dict(list(base_config.items()) + list(config.items()))

def get_encoder_layers(x, pool_length=2, nb_filter=40,
                       recurrent_output_size=60, num_blocks = 5, blocks_per_maxpooling = 1):

    # x = input_layer
    # x = LSTM(recurrent_output_size)(i)
    # x2 = Reshape((recurrent_output_size,1))(x)

    for i in range(num_blocks):
        a = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(),padding='same',name='block_conv_A_%d' %i)(x)
        d = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(),padding='same',name='block_conv_B_%d' %i)(a)
        x = Add()([a,d])
        # x = Dropout(0.25)(x)
        if (i+1) % blocks_per_maxpooling == 0:
            x = MaxPooling1D(pool_length)(x)


    return x

def get_decoder_layers(encoding_model,input_shape, pool_length=2, nb_filter=40,num_blocks = 5,blocks_per_maxpooling=1):

    x = encoding_model
    for i in range(num_blocks):
        a = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_A_%d' %i)(x)
        d = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_B_%d' %i)(a)
        x = Add()([a, d])
        if (i + 1) % blocks_per_maxpooling == 0:
            x = UpSampling1D(pool_length)(x)
    #Last layer

    # x = Conv1D(filters=input_shape[-1], kernel_size=5, activation='linear',
    #                  kernel_initializer=glorot_normal(), padding='causal', name='dilate_deconv')(x)
    x = BilinearUpsampling1D(input_shape[0])(x)
    decoded = Conv1D(filters=input_shape[-1], kernel_size=1, activation='linear',
           kernel_initializer=glorot_normal(), padding='same', name='final_deconv')(x)
    return decoded


def get_classification_layers(encoding_model,input_shape,num_outputs):
    x = Flatten(input_shape=input_shape)(encoding_model)
    y =  Dense(num_outputs, activation='sigmoid', kernel_initializer='glorot_uniform',name='classification_layer')(x)
    return y

def generate_dual_task_model(input_shape,num_outputs,pool_length=2, nb_filter=40,num_blocks = 5):
    input_layer = Input(shape=input_shape)
    encoder = get_encoder_layers(input_layer, pool_length=pool_length, nb_filter=nb_filter, num_blocks=num_blocks)
    decoder = get_decoder_layers(encoder,input_shape, pool_length=pool_length, nb_filter=nb_filter,num_blocks = num_blocks)
    classification_task = get_classification_layers(encoder,input_shape=input_shape,num_outputs=num_outputs)
    #Generate model
    model = Model(input_layer,[decoder,classification_task])
    model_enc = Model(input_layer, classification_task)
    # model_dec = Model(input_layer, decoder)
    model.summary()
    return model,model_enc,None

def generate_single_task_model(input_shape,num_outputs,pool_length=2, nb_filter=40,num_blocks = 5,weights_file=''):
    input_layer = Input(shape=input_shape)
    encoder = get_encoder_layers(input_layer, pool_length=pool_length, nb_filter=nb_filter, num_blocks=num_blocks)
    decoder = get_decoder_layers(encoder,input_shape, pool_length=pool_length, nb_filter=nb_filter,num_blocks = num_blocks)
    classification_task = get_classification_layers(encoder,input_shape=input_shape,num_outputs=num_outputs)
    #Generate model
    model = Model(input_layer,classification_task)
    model.summary()
    try:
        model.load_weights(weights_file,by_name=True)
    except:
        pass
    return model


if __name__ == "__main__":

    model, model_m2s, model_s2m = generate_dual_task_model((65,6), 8, pool_length=2, nb_filter=40, num_blocks=5)
    task2_loss_weight = 1.0
    sgd = optimizers.SGD(lr=0.001, decay=0.000001, momentum=0.9, nesterov=True)
    metrics = ['mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh','mean_squared_logarithmic_error', 'mse', 'mae', 'mape', 'logcosh']
    model.compile(optimizer=sgd,
                          loss=[losses.mean_absolute_error,gradient_regularization_l1_normalized],
                          loss_weights=[1., task2_loss_weight],
                          metrics=metrics)