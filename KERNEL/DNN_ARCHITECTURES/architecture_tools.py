# -*- coding: utf-8 -*-

#A class to support multiple NNs and/or other variables into a single structure. 
#If a single NN is used, NN_class and model_function_class may coincide.
class model_function_class():
   pass

#A class to save a NN
class NN_class():
    input_shape = None
    output_shape= None
    name = None
    architectures = None
