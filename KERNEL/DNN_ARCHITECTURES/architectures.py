# -*- coding: utf-8 -*-

#repository of architectures
import tensorflow as tf
from tensorflow.keras import layers

def architecture_test_2 (input_shape, output_shape):
    inputs = tf.keras.Input(shape=input_shape, name='Input')
    x = layers.Dense(4, activation='relu')(inputs)
    x = layers.Dense(2, activation='relu')(x)
    outputs = layers.Dense(output_shape[0], activation='relu')(x)
    return inputs, outputs