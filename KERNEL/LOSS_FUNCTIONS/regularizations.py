# -*- coding: utf-8 -*-
# !/usr/bin/python

import itertools
import numpy
import tensorflow as tf
import tensorflow.keras.backend as K
from NORMS.norms import abs_error_l1_norm_normalized

def gradient_y(y):

    mylist = []
    mylist.append(y[:, 0] - y[:, 2])
    mylist.append(y[:, 1] - y[:, 2])
    mylist.append(y[:, 2] - 0.5*y[:, 1] - 0.5*y[:, 0])
    mylist.append(y[:, 3])
    mylist.append(K.zeros_like(y[:, 4]))
    mylist.append(K.zeros_like(y[:, 4]))
    mylist.append(0.1*y[:, 6])

    grad_y = tf.stack(mylist)

    return grad_y

def gradient_regularization_l1_normalized(x_true, x_pred):
    assert K.ndim(x_true) == 2
    loss = K.mean(K.abs(gradient_y(x_pred)))
    return loss


def loss_l1_and_regularization_normalized(x_true, x_pred):
    assert K.ndim(x_true) == 2
    print('loss_l1_and_regularization_normalized.alpha=',loss_l1_and_regularization_normalized.alpha)
    loss = abs_error_l1_norm_normalized(x_true, x_pred) + \
             loss_l1_and_regularization_normalized.alpha * gradient_regularization_l1_normalized(x_true, x_pred)
    return loss


def loss_only_regularization_normalized(x_true, x_pred):
    assert K.ndim(x_true) == 2
    loss = loss_l1_and_regularization_normalized.alpha * gradient_regularization_l1_normalized(x_true, x_pred)
    print('loss_l1_and_regularization_normalized.alpha=',loss_l1_and_regularization_normalized.alpha)
    return loss

loss_l1_and_regularization_normalized.alpha = 0.5*6./8.