# -*- coding: utf-8 -*-
from DATA_PREPROCESSING.rescale import rescale_inverse

# Evaluate a DNN in the real scale given a trained model
def evaluate_dnn_real_scale(input_values, dnn_model, scaler, log_labels):

    # predict the values
    output_predicted = dnn_model.predict(input_values)

    # rescale the predicted and target variables to their real interval
    output_predicted = rescale_inverse(values=output_predicted, scaler=scaler, log_labels=log_labels)
    return output_predicted