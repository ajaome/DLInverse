# -*- coding: utf-8 -*-
import numpy
import tensorflow.keras.backend as K
import numpy

def mean_squared_error(x_true, x_pred):
    assert K.ndim(x_true) == 2
    return K.mean(K.square(x_pred - x_true), axis=-1)


def mean_absolute_error(x_true, x_pred):
    assert K.ndim(x_true) == 2
    return K.mean(K.abs(x_pred - x_true), axis=-1)

#see tf.norm to compute norms in tensors
# tf_norm=tf.norm(
#    tf_v,
#    ord=1,
#    axis=None,
#    keepdims=None,
#    name=None,
#    keep_dims=None
#)   


#def vector_norm_Lp(vector,p):
#    norm = (numpy.sum(numpy.power(numpy.abs(vector), p), dtype=numpy.float32))**(1./p)
#    return norm
#
#def matrix_norm_Lpq (matrix,p,q):
#    #column: axis = 0 ,  row: axis= 1
#    norm = numpy.sum(numpy.power(numpy.sum(numpy.power(numpy.abs(matrix), q), axis=0), (p/q))) ** (1./p)
#    return norm
#
#def norm_Frobenius(matrix): 
#    #result equal to matrix_norm_Lpq(matrix,2,2)
#    norm = numpy.sum(numpy.power(numpy.abs(matrix), 2))**(1./2)
#    return norm
##################################################################################
def abs_error_l1_norm_normalized(x_true,x_pred):
    assert K.ndim(x_true) == 2
    norm = K.mean(K.abs(x_true-x_pred), axis=-1)
    return norm


def l1_norm(x_true,x_pred):
    assert K.ndim(x_true) == 2
    loss_a = K.abs(x_true-x_pred)
    return loss_a

def l2_norm(x_true,x_pred):
    assert K.ndim(x_true) == 2
    loss_a = K.square(x_true-x_pred)
    return loss_a



def vector_norm_Lp(vector,p):
    norm = (numpy.sum(numpy.power(numpy.abs(vector), p), dtype=numpy.float32))**(1./p)
    return norm

def matrix_norm_Lpq (matrix,p,q):
    #column: axis = 0 ,  row: axis= 1
    norm = numpy.sum(numpy.power(numpy.sum(numpy.power(numpy.abs(matrix), q), axis=0), (p/q))) ** (1./p)
    return norm

def norm_Frobenius(matrix): 
    #result equal to matrix_norm_Lpq(matrix,2,2)
    norm = numpy.sum(numpy.power(numpy.abs(matrix), 2))**(1./2)
    return norm

##############################################################################
def compute_norm_by_column(values, norm_type):
    #Norms for numpy arrays
    #norm_type = p in the Lp norm
    
    if (norm_type == 1):
      #2D
      if len(values.shape)==2:
        norm = numpy.sum(abs(values), axis=0) 
      #3D
      if len(values.shape)==3:
        norm = numpy.sum(abs(values), axis=(0,1))
        
    elif (norm_type == 2):
      #2D
      if (len(values.shape) == 2):        
        norm=numpy.linalg.norm(values,2, axis=0)
      #3D
      if (len(values.shape) == 3):     
        norm=numpy.linalg.norm(values,2, axis=(0,1))               
    return norm

def compute_absolute_and_relative_errors(predicted_values, true_values, norm_type):
    #error control
    absolute_error = compute_norm_by_column(numpy.absolute(numpy.subtract(true_values, predicted_values)), norm_type)    
    relative_error = 100*numpy.divide(absolute_error,compute_norm_by_column(numpy.absolute(true_values)))
    return absolute_error, relative_error

def compute_mean_absolute_and_relative_errors (predicted_values, true_values, norm_type):
    absolute_error, relative_error= compute_absolute_and_relative_errors(predicted_values, true_values, norm_type)
    #obtain the number of entries
    if len(predicted_values)==2:
        num_data =  predicted_values.shape[1]
    if len(predicted_values)==3:
        num_data =  predicted_values.shape[1]*predicted_values.shape[2]
    #compute mean
    m_absolute_error = absolute_error / num_data
    m_relative_error = relative_error / num_data
    return m_absolute_error, m_relative_error

def compute_mean(values):

    # compute mean for 2D matrix
    if len(values.shape) == 2:
        final_mean = values.mean(axis=0)

    #compute mean for 3D matrix
    if len(values.shape) == 3:
        final_mean = values.mean(axis=(0, 1))
    return final_mean


def compute_absolute_and_relative_error(predicted_values, true_values):

    # compute errors
    absolute_error = numpy.absolute(numpy.subtract(true_values, predicted_values))
    mean_absolute_error = compute_mean(absolute_error)

    mean_true_values = compute_mean(numpy.absolute(true_values))
    mean_relative_error = numpy.divide(mean_absolute_error, mean_true_values)
    mean_relative_error = mean_relative_error*100
    return mean_absolute_error, mean_relative_error

#def compute_absolute_and_relative_error_pointwise(predicted_values, true_values):
#
#    # compute errors
#    absolute_error = numpy.absolute(numpy.subtract(true_values, predicted_values))
#
#    relative_error = numpy.divide(absolute_error, numpy.absolute(true_values))
#
#    # compute mean of the errors
#    mean_absolute_error = compute_mean(absolute_error)
#    mean_relative_error = compute_mean(relative_error)
#    mean_relative_error = mean_relative_error*100
#    return mean_absolute_error, mean_relative_error