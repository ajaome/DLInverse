# -*- coding: utf-8 -*-
import numpy
import copy

class NN_dataset():
    train = None
    validation=None
    test=None
    log_label=None
    names=None
    scaler=None
 
def make_input_output_list(mytuple):
    
    objective=NN_dataset()
    objective.train = []
    objective.validation = []
    objective.test = []
    objective.log_label = []
    objective.names = []
    objective.scaler = []     
    
    for element in mytuple:
        objective.train.append(element.train)
        objective.validation.append(element.validation) 
        objective.test.append(element.test) 
        objective.log_label.append(element.log_label) 
        objective.names.append(element.names) 
        objective.scaler.append(element.scaler) 
    return objective


def join_several_NN_dataset_classes(my_array):

    objective=NN_dataset()
    
    objective.train = my_array[0].train
    objective.validation = my_array[0].validation
    objective.test = my_array[0].test
    objective.log_label = my_array[0].log_label
    objective.names = copy.copy(my_array[0].names)
    objective.scaler = my_array[0].scaler

    for element in my_array[1:] :
        objective.train = numpy.concatenate((objective.train, element.train), axis=-1)
        objective.validation = numpy.concatenate((objective.validation, element.validation), axis=-1)
        objective.test = numpy.concatenate((objective.test, element.test), axis=-1)
        objective.log_label = numpy.concatenate((objective.log_label, element.log_label), axis=-1)
        objective.names.extend(element.names)
        objective.scaler = numpy.concatenate((objective.scaler, element.scaler), axis=-1)
    return objective