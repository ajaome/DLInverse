# -*- coding: utf-8 -*-
# !/usr/bin/python
import numpy
import joblib
from DATA_PREPROCESSING.linear_rescale import linear_rescale_forward, linear_rescale_inverse
from DATA_PREPROCESSING.log_rescale import log_rescale_forward, log_rescale_inverse

def rescale_forward(values, scaler, log_labels, scaler_interval):

    values_shape = values.shape
    values_rescaled = numpy.empty(values_shape)
    
    # lengths control
    if values_shape[-1] == len(log_labels):
        pass
    else:
       raise Exception('Error in rescale: length of log_labels (', len(log_labels), ') does not mach to the data length (',values_shape[-1] , ').' )
       
    # copy the matrix to a local variable
    if len(values_shape) == 2:
        values_rescaled[:, :] = values[:, :]
    else:
        values_rescaled[:, :, :] = values[:, :, :]

    # perform log rescale
    values_rescaled = log_rescale_forward(values_rescaled, log_labels)

    # Perform linear rescale
    scaler, values_rescaled = linear_rescale_forward(values_rescaled, scaler, scaler_interval=scaler_interval)
    return scaler, values_rescaled

def rescale_inverse(values, scaler, log_labels):

    values_shape = values.shape
    values_real_scale = numpy.empty(values_shape)
    
    # lengths control
    if values_shape[-1] == len(log_labels):
        pass
    else:
       raise Exception('Error in rescale: length of log_labels (', len(log_labels), ') does not mach to the data length (',values_shape[-1] , ').' )

    # copy the matrix to a local variable
    if len(values_shape) == 2:
        values_real_scale[:, :] = values[:, :]
    else:
        values_real_scale[:, :, :] = values[:, :, :]

    # inverse linear rescale
    values_real_scale = linear_rescale_inverse(values_real_scale, scaler)

    # perform log rescale
    values_real_scale = log_rescale_inverse(values_real_scale, log_labels)

    return values_real_scale

# save the rescalers
def save_rescalers (scalers, scaler_filename='./scalers/scaler'):

    for iscaler in range(len(scalers)):
        # save the scalers
        joblib.dump(scalers[iscaler], scaler_filename + '_' + str(iscaler)+'.pic')

# load the rescalers
def load_rescalers (scalers_dimension, scaler_filename='./scalers/scaler'):
    scalers = []

    # loop through number of scalers
    print('scaler dimension', scalers_dimension)
    for iscaler in range(scalers_dimension):
        # load rescalor
        print ('loading', scaler_filename + '_' + str(iscaler)+'.pic')
        scalers.append(joblib.load(scaler_filename + '_' + str(iscaler)+'.pic'))
    print (scalers)
    return scalers



