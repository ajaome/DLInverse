# -*- coding: utf-8 -*-
# !/usr/bin/python
import numpy

def log_rescale_forward(values, log_labels=None):

    values_shape = values.shape
    values_rescaled = numpy.empty(values_shape)

    if log_labels is not None:
        # if we have 2D matrix
        if len(values_shape) == 2:
            values_rescaled[:, :] = values[:, :]

            # Perform log rescale for the desired variables
            for ifeature in range(values_shape[1]):
                if log_labels[ifeature] == 1:
                    values_rescaled[:, ifeature] = numpy.log10(values_rescaled[:, ifeature])

        # if we have 3D matrix
        else:
            values_rescaled[:, :, :] = values[:, :, :]

            # Perform log rescale for the desired variables
            for ifeature in range(values_shape[2]):
                if log_labels[ifeature] == 1:
                    print( values_rescaled[:, :, ifeature])
                    values_rescaled[:, :, ifeature] = numpy.log10(values_rescaled[:, :, ifeature])

    return values_rescaled

def log_rescale_inverse(values, log_labels=None):

    values_shape = values.shape
    values_real_scale = numpy.empty(values_shape)

    if log_labels is not None:
        # if we have 2D matrix
        if len(values_shape) == 2:
            values_real_scale[:, :] = values[:, :]

            # Perform log rescale for the desired variables
            for ifeature in range(values_shape[1]):
                if log_labels[ifeature] == 1:
                    values_real_scale[:, ifeature] = 10**values_real_scale[:, ifeature]

        # if we have 3D matrix
        else:
            values_real_scale[:, :, :] = values[:, :, :]
            # Perform log rescale for the desired variables
            for ifeature in range(values_shape[2]):
                if log_labels[ifeature] == 1:
                    values_real_scale[:, :, ifeature] = 10**values_real_scale[:, :, ifeature]

    return values_real_scale
