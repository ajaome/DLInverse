# -*- coding: utf-8 -*-
# !/usr/bin/python

from sklearn.preprocessing import MinMaxScaler
import numpy

def linear_rescale_forward(values, scalers, scaler_interval=[0.5,1.5]):

    values_shape = values.shape
    values_rescaled = numpy.empty(values_shape)

    if len(values_shape) == 2:
        values_rescaled[:, :] = values[:, :]
        # if the scalers were undefined
        if scalers is None:
            scalers = []
            scaler_one_variable = MinMaxScaler(feature_range=scaler_interval)
            scaler_one_variable = scaler_one_variable.fit(values_rescaled)
            scalers.append(scaler_one_variable)

        # transform the data to the desired interval
        scaler_one_variable = scalers[0]
        values_rescaled = scaler_one_variable.transform(values_rescaled)

    else:
        values_rescaled[:, :, :] = values[:, :, :]
        # if the scalers has not built before
        if scalers is None:
            scalers = []
            for ivariable in range(values_shape[2]):
                values_one_variable = values_rescaled[:, :, ivariable]
                values_one_variable = values_one_variable.reshape([values_shape[0]*values_shape[1], 1])
                scaler_one_variable = MinMaxScaler(feature_range=scaler_interval)
                scaler_one_variable = scaler_one_variable.fit(values_one_variable)
                scalers.append(scaler_one_variable)

        #transform the data to the desired interval
        for ivariable in range(values_shape[2]):
            values_one_variable = values_rescaled[:, :, ivariable]
            values_one_variable = values_one_variable.reshape([values_shape[0]*values_shape[1], 1])
            scaler_one_variable = scalers[ivariable]
            values_one_variable = scaler_one_variable.transform(values_one_variable)
            values_one_variable = values_one_variable.reshape([values_shape[0], values_shape[1], 1])
            values_rescaled[:, :, ivariable] = values_one_variable[:, :, 0]

    return scalers, values_rescaled

def linear_rescale_inverse(values, scalers):

    values_shape = values.shape
    values_real_scale = numpy.empty(values_shape)
    # For a 2D matrix, we build a set of scalers
    if len(values_shape) == 2:
        values_real_scale[:, :] = values[:, :]

        # transform the data to the desired interval
        scaler_one_variable = scalers[0]
        values_real_scale = scaler_one_variable.inverse_transform(values_real_scale)
    # For a 3D matrix we build a set of scalers
    else:
        values_real_scale[:, :, :] = values[:, :, :]

        #transform the data to the desired interval
        for ivariable in range(values_shape[2]):
            values_one_variable = values_real_scale[:, :, ivariable]
            values_one_variable = values_one_variable.reshape([values_shape[0]*values_shape[1], 1])
            scaler_one_variable = scalers[ivariable]
            values_one_variable = scaler_one_variable.inverse_transform(values_one_variable)
            values_one_variable = values_one_variable.reshape([values_shape[0], values_shape[1], 1])
            values_real_scale[:, :, ivariable] = values_one_variable[:, :, 0]

    return values_real_scale







