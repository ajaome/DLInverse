# -*- coding: utf-8 -*-
# !/usr/bin/python
import os
import shutil

class automatic_report():
   generation = None
   
   
def generate_tex (output_path, experiment_name, plot_names, log_labels, author):
    print("report under construction...")
    
    tex_path=write_preamble(output_path, experiment_name, author)
    write_experiment_information(tex_path, output_path, experiment_name)
    write_ground_truth("train", tex_path, output_path, plot_names)
    write_ground_truth("validation", tex_path, output_path, plot_names)
    write_ground_truth("test", tex_path, output_path, plot_names)
    write_figure_model(tex_path, output_path)
        
    file = open(tex_path, "a")
    file.write(r"\end{document}"+ "\n"     ) 
    file.close()
    
    #compile tex
    os.system("pdflatex "+tex_path)
    #copy and remove files generated
    shutil.copy("./report.pdf", output_path + "/latex")
    shutil.copy("./report.log", output_path + "/latex")
    shutil.copy("./report.aux", output_path + "/latex")
    os.remove("./report.log")
    os.remove("./report.aux")
    print("")
    print("Report.pdf generated")
    return


def write_preamble (output_path, experiment_name, author):
    tex_path=(output_path +"/latex/report.tex")
    file = open(tex_path,"w") 
    
    file.write(r"\documentclass[a4paper,11pt]{article}"+ "\n"           )            
    file.write(r"\usepackage[utf8]{inputenc}"+ "\n"                     )
    file.write(r"\usepackage{amssymb,amsmath,amsfonts,mathrsfs}"+ "\n"  )
    file.write(r"\usepackage{latexsym}"+ "\n"                           )
    file.write(r"\usepackage[useregional]{datetime2}         "+ "\n"    )
    file.write(r"\usepackage{graphicx}         "+ "\n"    )
    file.write(r"\usepackage{subcaption}         "+ "\n"    )
    file.write(r"         "+ "\n"    ) 
    file.write(r"         "+ "\n"    ) 
    file.write(r"\title{\textnormal{\huge{Report of experiment: " + experiment_name.replace("_", "\_")+ "}}}"+ "\n")
    file.write(r"\author{"+author+"}"+ "\n"     )     
    file.write(r"\date{\today}"+ "\n"     )     
    file.write(r"         "+ "\n"    )    
    file.write(r"\begin{document}"+ "\n" ) 
    file.write(r"\maketitle"+ "\n"       )
    file.write(r"         "+ "\n"    )  
    file.write(r"\captionsetup[subfigure]{labelformat=empty, labelsep=none}"+ "\n"    )  
    
    file.close()
    
    return tex_path

def write_experiment_information(tex_path, output_path, experiment_name):
    file = open(tex_path, "a")
    file.write(r"         "+ "\n"    ) 
    file.write(r"\newpage"+ "\n"    ) 
    file.write(r"\section{Experiment configuration}"+ "\n"  )
    file.write(r"texttttt "+ "\n"     )
    file.write(r"         "+ "\n"    ) 
    
    #figure loss
    file.write(r"\begin{figure}[h!]"+ "\n"    )     
    file.write(r"\centering"+ "\n"    )
    file.write(r"\includegraphics[width=\textwidth]{"+ output_path +r"/model_files/history/"+experiment_name + "_history.png}"+ "\n"    )    
#    file.write(r"\includegraphics[width=\textwidth]{"+ output_path +r"/model_files/history/"+experiment_name[0:experiment_name.find("_n")]+ "_history.png}"+ "\n"    )
    file.write(r"\end{figure}"+ "\n"    )
    file.write(r"         "+ "\n"    )
    
    file.close()

def write_figure_model(tex_path, output_path):
    file = open(tex_path, "a")
    file.write(r"\newpage"+ "\n"    )    
    file.write(r"\begin{figure}[h!]"+ "\n"    )     
    file.write(r"\centering"+ "\n"    )    
    file.write(r"\includegraphics[height=0.93\textheight]{"+ output_path +r"/model_files/model_plot.png}"+ "\n"    )
    file.write(r"\caption{Neural Network Architecture}"+ "\n"    )
    file.write(r"\end{figure}"+ "\n"    )      
    file.write(r"         "+ "\n"    ) 
    file.close()    
    return

def write_ground_truth(dataset, tex_path, output_path, plot_names):
    file = open(tex_path, "a")
    file.write(r"         "+ "\n"    ) 
    file.write(r"\newpage"+ "\n"    ) 
    file.write(r"\section{Ground-truth vs predicted values: "+dataset+ " dataset}"+ "\n"  )

    file.write(r"\begin{figure}[h!]"+ "\n"    )     
    file.write(r"\centering"+ "\n"    )     

    for image in plot_names:
        file.write(r"\begin{subfigure}[b]{0.45\textwidth}"+ "\n"    ) 
        file.write(r"\includegraphics[width=\textwidth]{"+ output_path +r"/"+ dataset+"/"+image + ".png}"+ "\n"    ) 
        file.write(r"\caption{ }"+ "\n"    )
        file.write(r"\end{subfigure}"+ "\n"    )

    file.write(r"\end{figure}"+ "\n"    ) 
    file.close()
    return



#only to build fast the pdf in tests    
def prueba(path):
    author = "Á. J. Omella"
    experiment_name = "AJO_algorithm_202_loss_forward_n10"
    output_path =path+"/"+ experiment_name
    plot_names = ['Atten-Azim', 'Phase-Azim', 'Atten-Geosignal',
                                 'Phase-Geosignal', 'Atten-Coaxial', 'Phase-Coaxial']
    log_labels = [0,0,0,0,0,0]
    generate_tex (output_path, experiment_name, plot_names, log_labels, author)
    return