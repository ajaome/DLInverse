# -*- coding: utf-8 -*-
# !/usr/bin/python
import os
import numpy


def write_measurements_into_file(materials, measurements, output_directory):

    #write obtained measurements into test files
    for imodel in range(materials.shape[0]):
        output_filename = output_directory + '/R%010d.dat' % (imodel + 1)
        f = open(output_filename, "w")
        trajectory_information = materials[imodel, 7:9]
        trajectory_information = 180*trajectory_information/3.1416
        f.write(str(trajectory_information[0]) + "    " + str(trajectory_information[1]) + "\n")
        for ilog_pos in range(measurements.shape[1]):
            f.write(str(measurements[imodel,ilog_pos,0]) + "    " + str(measurements[imodel,ilog_pos,1]) + "\n")

        for ilog_pos in range(measurements.shape[1]):
            f.write(str(measurements[imodel,ilog_pos,2]) + "    " + str(measurements[imodel,ilog_pos,3]) + "\n")

        for ilog_pos in range(measurements.shape[1]):
            f.write(str(measurements[imodel,ilog_pos,4]) + "    " + str(measurements[imodel,ilog_pos,5]) + "\n")
        f.close()
    return

###############################################################################
### relative and absolute error
###############################################################################
def write_relative_absolute_error_to_files(relative_error, absolute_error, output_path):
    write_relative_error_to_file(relative_error=relative_error, results_directory=output_path)
    write_absolute_error_to_file(absolute_error=absolute_error, results_directory=output_path)
    return


def write_relative_error_to_file(relative_error, results_directory):

#   make the results directory
    try:
        os.makedirs(results_directory+'/relative_error')
    except:
        pass

#   loop through the relative error of each measurement
    for i in range(relative_error.shape[0]):

#       open one file for each measurement
        file_name = results_directory + '/relative_error/Relative_error_component_' + str(i) + '.dat'
        with open(file_name, 'a') as error_file:
            error_file.write(str(relative_error[i]) + '\n')
            error_file.close()
    return

def write_absolute_error_to_file(absolute_error, results_directory):

#   make the results directory
    try:
        os.makedirs(results_directory+'/absolute_error')
    except:
        pass

#   loop through the relative error of each measurement
    for i in range(absolute_error.shape[0]):

#       open one file for each measurement
        file_name = results_directory + '/absolute_error/Absolute_error_component_' + str(i) + '.dat'
        with open(file_name, 'a') as error_file:
            error_file.write(str(absolute_error[i]) + '\n')
    return
###############################################################################
### 
###############################################################################
def write_results_in_csv_file(values, results_directory, csv_filename, labels=''):

    # if we have 2D matrix
    if (len(values.shape) == 2):
        numpy.savetxt(results_directory+'/'+csv_filename+'.csv', values, delimiter=",")

    # if we have 3D matrix. For each type of value we have one csv file
    else:
        one_measurement = numpy.empty([values.shape[0], values.shape[1]])

        for ivalue in range(values.shape[2]):
            one_measurement[:,: ] = values[:, :, ivalue]
            numpy.savetxt(results_directory+'/'+csv_filename+'_'+labels[ivalue]+'.csv',
                          one_measurement, delimiter=",")
    return

def write_ground_predicted_files(predicted, ground_truth, output_path, measurements_labels):
    #save the files
    write_results_in_csv_file(values=predicted, results_directory=output_path,
                              csv_filename='predicted_values', labels=measurements_labels)

    write_results_in_csv_file(values=ground_truth, results_directory=output_path,
                              csv_filename='ground_truth', labels=measurements_labels)    
    return