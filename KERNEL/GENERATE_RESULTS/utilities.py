# -*- coding: utf-8 -*-
# !/usr/bin/python
import os

def make_results_directories(output_path):
    try:
        os.makedirs(output_path)
    except:
        pass

    try:
        os.makedirs(output_path+'/train')
    except:
        pass

    try:
        os.makedirs(output_path+'/validation')
    except:
        pass

    try:
        os.makedirs(output_path+'/test')
    except:
        pass
    
#    try:
#        os.makedirs(output_path+'/latex')
#    except:
#        pass
    
    try:
        os.makedirs(output_path+'/model_files')
    except:
        pass
  
    return


def load_header(filename, number_output_columns, header_delimiters_list):
    with open(filename) as f:
        first_line = f.readline()
    #remove things
    first_line = first_line.replace(' ',"")
    first_line = first_line.replace('/',"-")
    first_line = first_line.replace("\r","")
    first_line = first_line.replace("\n","")
    #separate
    counter = 0
    header = [None] * number_output_columns
    for char in first_line:
        if char == " ": pass
        elif char in header_delimiters_list:
            counter = counter + 1
        else:
           if header[counter] is None:
               header[counter]= str(char)
           else:
               header[counter] +=  str(char)
    return header