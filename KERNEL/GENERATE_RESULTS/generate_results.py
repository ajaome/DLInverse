# -*- coding: utf-8 -*-
# !/usr/bin/python
import numpy
from GENERATE_RESULTS.write_results_into_files import write_relative_absolute_error_to_files, write_ground_predicted_files
from NORMS_AND_ERRORS.compute_errors import compute_absolute_and_relative_error

from GENERATE_RESULTS.utilities import load_header
from GENERATE_RESULTS.generate_graphics import plot_history

#import matplotlib.pyplot as plot

def evaluate_relative_and_absolute_error(NNinput, NNoutput, my_model):
    NNpredicted = my_model.predict(NNinput)

    absolute_error, relative_error = compute_absolute_and_relative_error(NNpredicted, NNoutput)
    return absolute_error, relative_error

def evaluate_realative_and_absolute_errors_all_data_partitions(my_model, x_train, y_train,x_validation, y_validation):
    absololute_error_training, relative_error_training =\
        evaluate_relative_and_absolute_error(x_train,y_train, my_model)

    absololute_error_validation, relative_error_validation =\
        evaluate_relative_and_absolute_error(x_validation, y_validation, my_model)

    return absololute_error_training, relative_error_training ,\
           absololute_error_validation, relative_error_validation


def save_load_history(path, experiment_name,new_epochs,new_lr, new_logs, graphic_configuration):
    fname= path+'/history/'+experiment_name+'_history.csv'
    
    new_values = numpy.array(new_lr)    
    for item in new_logs:
        new_values= numpy.vstack((new_values, new_logs[item]))
    new_values = new_values.T
    
    try: 
       #load file
       old_values = numpy.loadtxt(fname, delimiter="\t", skiprows=1)
       header_delimiters_list=["\t"]
       header_list =load_header(fname, len(old_values[0]), header_delimiters_list)
       header ='\t'.join(header_list)
       first_epoch = new_epochs[0]
       old_epochs= old_values.T[0]
       epochs=numpy.concatenate((old_epochs[:first_epoch],numpy.asarray(new_epochs)),axis=0).astype(int)       
       old_values = numpy.delete(old_values, 0, 1)
       values = numpy.concatenate((old_values[:][:first_epoch], new_values), axis=0)
      
    except:
        header="epoch \t learning_rate"
        epochs = new_epochs
        values=new_values
        for item in new_logs:
            header= header + " \t " + item
   
    #write the file
    with open(fname, "w") as f:
        f.write(header)
        f.write('\n')
        for (epoch, line) in zip(epochs,values):
            f.write(str(epoch))
            for data in line:
                f.write('\t'+ str(data))
            f.write('\n')
    #plot     
    plot_history(epochs, values.T, header, path+'/history/'+experiment_name, graphic_configuration)
    return

def save_error_header(filename, names):
    with open(filename, 'w') as f:
        f.write("epoch")
        for name in names:
            f.write ("\t" + name)
        f.write ("\n")

def save_error_line (filename, epoch, errors):
    with open(filename, 'a') as f:
        f.write(str(int(epoch)))
        for error in errors:
            f.write ("\t" + str(error))
        f.write ("\n")