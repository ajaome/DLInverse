# -*- coding: utf-8 -*-
import matplotlib as mpl
from sklearn.metrics import mean_squared_error
import scipy
import seaborn
import pandas
import os.path
import numpy
import matplotlib.pyplot as plt
from matplotlib import rc
from DATA_PREPROCESSING.linear_rescale import linear_rescale_inverse
from GENERATE_RESULTS.write_results_into_files import write_ground_predicted_files
from GENERATE_RESULTS.utilities import load_header
from matplotlib.ticker import MaxNLocator

# mpl.use('Agg')


def plot_predicted_values_and_ground_truth_all_variable(predicted_values, ground_truth,
                                                        output_directory, figure_labels):
    # for forward problem
    if len(ground_truth.shape) == 3:
        #   Plot real and imaginary part separately
        for i in range(ground_truth.shape[2]):
            one_measurement_true = ground_truth[:, :, i]
            one_measurement_true = one_measurement_true.reshape([one_measurement_true.shape[0] *
                                                                 one_measurement_true.shape[1]])
            one_measurement_pred = predicted_values[:, :, i]
            one_measurement_pred = one_measurement_pred.reshape([one_measurement_pred.shape[0] *
                                                                 one_measurement_pred.shape[1]])

            plot_predicted_values_vs_ground_truth(one_measurement_true[:], one_measurement_pred[:], figure_labels[i],
                                                  output_directory + '/' + figure_labels[i] + '.png')
            plot_predicted_values_vs_ground_truth(one_measurement_true[:], one_measurement_pred[:], figure_labels[i],
                                                  output_directory + '/' + figure_labels[i] + '.pdf')

    else:
        for i in range(ground_truth.shape[1]):
            plot_predicted_values_vs_ground_truth(ground_truth[:, i], predicted_values[:, i],
                                                  figure_labels[i], output_directory + '/' + figure_labels[i] + '.png')
            plot_predicted_values_vs_ground_truth(ground_truth[:, i], predicted_values[:, i],
                                                  figure_labels[i], output_directory + '/' + figure_labels[i] + '.pdf')
    # close the pyplot session
    plt.clf()
    plt.cla()
    plt.close()


def plot_predicted_values_vs_ground_truth(gt_array, pred_array, title_label, filename):
    min_val = numpy.min((gt_array.min(), pred_array.min()))
    max_val = numpy.max((gt_array.max(), pred_array.max()))
    increment = max_val - min_val

    limits = (min_val - 0.05 * increment, max_val + 0.05 * increment)

    x, y = pandas.Series(gt_array, name="Ground Truth"), pandas.Series(pred_array, name="Predicted")

    g = seaborn.jointplot(x=x, y=y, kind="hex", color="#5d5d60", joint_kws={'gridsize': 40, 'bins': 'log'}, xlim=limits,
                          ylim=limits, stat_func=None)
    # seaborn.regplot(pandas.Series(numpy.arange(xlim[0], xlim[1], 0.01)), pandas.Series(numpy.arange(xlim[0], xlim[1], 0.01)), ax=g.ax_joint, scatter=False)
    g.ax_joint.plot(numpy.linspace(limits[0], limits[1]), numpy.linspace(limits[0], limits[1]))
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(gt_array, pred_array)

    g.fig.suptitle(title_label, y=0.99)

    textstr = '\n'.join((
        #    r'$\mathrm{rms}=%.2f$' % (rms, ),
        #    '# data ='  % (numpy.size(gt_array), ),
        r'$\mathrm{r^2}=%.4f$' % (r_value ** 2,),
        #    r'$\mathrm{p}=%.2f$' % (p_value, ),
    ))
    #    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', alpha=0.5, facecolor='none')
    #    # place a text box in upper left in axes coords
    g.ax_joint.text(0.05, 0.95, textstr, transform=g.ax_joint.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

    plt.tight_layout()
    # plt.show()
    if filename.endswith(".png"):
        g.fig.savefig(filename, dpi=600)
    else:
        g.fig.savefig(filename)
    plt.close('all')
    # close the pyplot session
    plt.clf()
    plt.cla()
    plt.close()


def plot_error(output_names, results_directory, file_name):
    error_shape = len(output_names)
    #   loop through all the measurements
    for i in range(error_shape):

        #       load already saved relative errors from text file
        text_file_name = results_directory + file_name + '_' + output_names[i] + '.dat'
        lines = [line.rstrip('\n') for line in open(text_file_name)]
        error_length = len(lines)
        error = numpy.empty([error_length, 2])
        error[:, 1] = numpy.asarray(lines)
        for j in range(error_length):
            error[j, 0] = j

        #       plot the relative error and save it
        x = error[:, 0]
        y = error[:, 1]
        plt.plot(x, y, 'k')

        fig_filename = results_directory + file_name + '_' + output_names[i] + '.png'
        plt.savefig(fig_filename)

        fig_filename_pdf = results_directory + file_name + '_' + output_names[i] + '.pdf'
        plt.savefig(fig_filename_pdf)

        #       close the pyplot session
        plt.clf()
        plt.cla()
        plt.close()

    all_errors = numpy.empty([error_shape, error_length])
    #   loop through all the measurements to save all of them in one array
    for i in range(error_shape):
        # load already saved relative errors from text file
        text_file_name = results_directory + file_name + '_' + output_names[i] + '.dat'
        lines = [line.rstrip('\n') for line in open(text_file_name)]
        all_errors[i, :] = numpy.asarray(lines)

    return all_errors


def plot_error_double_value(error_training, error_validation, output_names, output_path, error_type):
    #   make the results directory
    if error_type == 'absolute':
        error_output_path = output_path + '/train_val_absolute_error'
        try:
            os.makedirs(error_output_path)
        except:
            pass

    else:
        error_output_path = output_path + '/train_val_relative_error'
        try:
            os.makedirs(error_output_path)
        except:
            pass

    error_length = error_training.shape[0]
    x = numpy.empty([error_training.shape[1]])
    for i in range(error_training.shape[1]):
        x[i] = i

    #   loop through all the measurements and read the training erros
    for i in range(error_length):
        #       plot the relative error and save it
        y1 = error_training[i, :]
        y2 = error_validation[i, :]
        plt.plot(x, y1, 'k', label='Training')
        plt.plot(x, y2, 'r--', label='Validation')

        plt.legend(loc='upper right')
        plt.xlabel("Iterations (number)")
        if (error_type == 'absolute'):
            plt.yscale('log')
            plt.ylabel("Absolute error")
        else:
            plt.ylabel('Relative error (percentage)')

        plt.gcf().subplots_adjust(left=0.15)

        fig_filename = error_output_path + '/' + error_type + '_' + output_names[i] + '.png'
        plt.savefig(fig_filename)

        fig_filename_pdf = error_output_path + '/' + error_type + '_' + output_names[i] + '.pdf'
        plt.savefig(fig_filename_pdf)

        #       close the pyplot session
        plt.clf()
        plt.cla()
        plt.close()


def plot_errors(output_path, outputs_names):
    # draw relative error
    file_name = 'relative'
    results_directory = output_path + '/train/'

    relative_error_training = plot_error(output_names=outputs_names,
                                         results_directory=results_directory,
                                         file_name=file_name)

    results_directory = output_path + '/validation/'

    relative_error_val = plot_error(output_names=outputs_names,
                                    results_directory=results_directory,
                                    file_name=file_name)

    # draw relarive error for train and validation together
    plot_error_double_value(error_training=relative_error_training, error_validation=relative_error_val,
                            output_names=outputs_names, output_path=output_path, error_type='relative')

    # draw absolute error
    file_name = 'abs'
    results_directory = output_path + '/train/'

    absolute_error_training = plot_error(output_names=outputs_names,
                                         results_directory=results_directory,
                                         file_name=file_name)

    results_directory = output_path + '/validation/'

    absolute_error_val = plot_error(output_names=outputs_names,
                                    results_directory=results_directory,
                                    file_name=file_name)

    # draw absolute error for train and validation together
    plot_error_double_value(error_training=absolute_error_training, error_validation=absolute_error_val,
                            output_names=outputs_names, output_path=output_path, error_type='absolute')
    return


def plot_ground_truth_vs_predicted(input_scaled_values, true_scaled_output, my_model, output_scalers,
                                   my_output_path, output_labels):
    # evaluate the trained dnn inside the interval
    predicted = my_model.predict(input_scaled_values)
    #####
    ####bug loglabels in names if log
    # linear inverse rescale of predicted and truth values
    predicted = linear_rescale_inverse(values=predicted, scalers=output_scalers)
    ground_truth = linear_rescale_inverse(values=true_scaled_output, scalers=output_scalers)
    print("plotting ground truth vs predicted values")
    #    # plot predicted values vs ground truth
    plot_predicted_values_and_ground_truth_all_variable(predicted_values=predicted,
                                                        ground_truth=ground_truth,
                                                        output_directory=my_output_path,
                                                        figure_labels=output_labels)

    write_ground_predicted_files(predicted, ground_truth, my_output_path, output_labels)
    return


def plot_ground_truth_vs_predicted_all_data_partitions(training_input, trainining_output,
                                                       val_input, val_output,
                                                       test_input, test_output,
                                                       model, path,
                                                       output_scalers, output_labels):
    plot_ground_truth_vs_predicted(training_input, trainining_output, model, output_scalers,
                                   path + '/train', output_labels)

    plot_ground_truth_vs_predicted(val_input, val_output, model, output_scalers,
                                   path + '/validation', output_labels)

    plot_ground_truth_vs_predicted(test_input, test_output, model, output_scalers,
                                   path + '/test', output_labels)

    return



def plot_history(epochs, values, header, part_filename, graphic_configuration): 
    # plt.style.use('seaborn-paper')
    
    # plot learning_rates and save figure
    fig, ax = plt.subplots()
    
    ax.plot(epochs, values[0])
    plt.xlabel('Epoch', fontsize=18)
    plt.ylabel('Learning rate', fontsize=18)
    ax.set_yscale('log')
    # ax.ticklabel_format(style='sci', axis='y', scilimits=(0.1, 10), useMathText=True)
    plt.tight_layout()
    if graphic_configuration.png:
        fig.savefig(part_filename + "_lrates.png", dpi=600)
    if graphic_configuration.pdf:
        fig.savefig(part_filename + "_lrates.pdf")
    plt.clf()
    plt.cla()
    plt.close()

    # plot loss and metrics
    jump = int((len(values) - 1) / 2)
    header_list = header.split("\t")
    header_list = header_list[2:jump + 2]
    i = 1
    for item in header_list:
        validation = values[i + jump]
        train = values[i]
        
        fig, ax = plt.subplots()
        ax.plot(epochs, validation, label="validation",  color='black',  linewidth= 0.8)
        ax.plot(epochs, train, label="train",  color='red', linewidth= 0.8)
        plt.xlabel('Epoch', fontsize=18)
        plt.ylabel(item, fontsize=18)
        if graphic_configuration.loss_scale == "log":
            ax.set_yscale('log')
        if graphic_configuration.loss_scale == "symlog": 
            ax.set_yscale('symlog', nonposy='clip', linthreshy=graphic_configuration.linthreshy)
        # ax.set_yscale('symlog', nonposy='clip', linthreshy=0.000000001)
        ax.legend(loc='best')
        plt.tight_layout()
        if graphic_configuration.png:
            fig.savefig(part_filename + "_" + item + ".png", dpi=600)
        if graphic_configuration.png:
            fig.savefig(part_filename + "_" + item +'.pdf')
        plt.clf()
        plt.cla()
        plt.close()
        i = i + 1
    return


def plot_new_error(output_path, training_error_path, validation_error_path, label, headers):
    try:
        os.makedirs(output_path + '/error_plots')
    except:
        pass

    training_file = training_error_path + label + '.dat'
    validation_file = validation_error_path + label + '.dat'

    error_train = numpy.loadtxt(training_file, delimiter="\t", skiprows=1)
    error_validation = numpy.loadtxt(validation_file, delimiter="\t", skiprows=1)
    # epoch vector
    epoch = (error_train.T[0]).astype(int)
    # header epoch
    #    headers = load_header(training_file, len(error_train[0]), ['\t'])
    #    headers = headers[1:]

    for train_line, validation_line, header in zip(error_train.T[1:], error_validation.T[1:], headers):
        plt.title(r'$ %s $' % (header))
        plt.plot(epoch, train_line, 'k', label='Training')
        plt.plot(epoch, validation_line, 'r--', label='Validation')
        plt.legend(loc='upper right')
        plt.xlabel("Iterations (number)")
        if (label=='absolute_error'):
            plt.yscale('log')
            plt.ylabel("Absolute error")
        else:
            plt.ylabel('Relative error (percentage)')

        plt.gcf().subplots_adjust(left=0.15)
        plt.gca().xaxis.set_major_locator(MaxNLocator(integer=True))
        fig_filename = output_path + '/error_plots/' + header + '_' + label + '.png'
        plt.savefig(fig_filename)
        fig_filename_pdf = output_path + '/error_plots/' + header + '_' + label + '.pdf'
        plt.savefig(fig_filename_pdf)

        #       close the pyplot session
        plt.clf()
        plt.cla()
        plt.close()
