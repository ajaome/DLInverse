# -*- coding: utf-8 -*-
import tensorflow as tf

def restringe_to_one_GPU_and_limit_memory(GPU_number, memory_limit):
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
      # Restrict TensorFlow to only allocate memory_limit of memory on the GPU_number GPU
      try:
        tf.config.experimental.set_virtual_device_configuration(
            gpus[GPU_number],
            [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=memory_limit)])
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
      except RuntimeError as e:
        # Virtual devices must be set before GPUs have been initialized
        print(e)
        
    
def limit_CPU(num_threads_inter, num_threads_intra):
    tf.config.threading.set_inter_op_parallelism_threads(num_threads_inter)
    tf.config.threading.set_intra_op_parallelism_threads(num_threads_intra)
    return


def limit_roK(GPU_number = 0):
    limit_CPU(1,1)
    restringe_to_one_GPU_and_limit_memory(GPU_number, memory_limit = 8192)
    return
