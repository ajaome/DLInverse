# -*- coding: utf-8 -*-
# !/usr/bin/python

from GENERATE_RESULTS.generate_graphics import plot_ground_truth_vs_predicted_all_data_partitions
from DNN_TRAINING.save_load_model import load_architecture, search_last_epoch
import tensorflow as tf
from DNN_TRAINING_USER.my_model import BilinearUpsampling1D, costum_sigmoid
from DATA_PREPROCESSING.linear_rescale import linear_rescale_inverse
from GENERATE_RESULTS.generate_graphics import plot_predicted_values_and_ground_truth_all_variable
from GENERATE_RESULTS.write_results_into_files import write_ground_predicted_files
from tensorflow.keras.models import load_model


def my_postprocessing_submodel(experiment_name_base, NN_input_train, NN_output_train, NN_input_validation,
                               NN_output_validation, NN_input_test, NN_output_test, NN_output_scaler,
                               NN_output_names,model_name,submodel_name, number_samples, results_path):

    experiment_name = experiment_name_base + '_%s' % (model_name)
    output_path = results_path + '/' + experiment_name + '_dataset%d' % number_samples
    model_folder = output_path + '/model_files/h5/' + experiment_name
    # change in the future, think to insert in the class my_custom_objects
    last_epoch = search_last_epoch(output_path, experiment_name)
    my_model = tf.keras.models.load_model(model_folder+'_'+submodel_name+'_epoch_' + str(last_epoch) + '.h5',
                                       custom_objects={'costum_sigmoid': costum_sigmoid,
                                                       'BilinearUpsampling1D': BilinearUpsampling1D})

    # evaluate the DNN on training, validation, and test datasets and save the results
    plot_ground_truth_vs_predicted_all_data_partitions(training_input=NN_input_train,
                                                       trainining_output=NN_output_train,
                                                       val_input=NN_input_validation,
                                                       val_output=NN_output_validation,
                                                       test_input=NN_input_test,
                                                       test_output=NN_output_test,
                                                       model=my_model,
                                                       path=output_path,
                                                       output_scalers=NN_output_scaler,
                                                       output_labels=NN_output_names)

    del my_model
    return


def plot_ground_truth_vs_predicted_all_data_partitions_user(training_input, trainining_output,
                                                       val_input, val_output,
                                                       test_input, test_output,
                                                       model, path,
                                                       output_scalers,  output_labels):

    plot_ground_truth_vs_predicted_user(training_input, trainining_output, model, output_scalers,
                                   path+'/train', output_labels)
    #
    plot_ground_truth_vs_predicted_user(val_input, val_output, model, output_scalers,
                                   path+'/validation', output_labels)

    plot_ground_truth_vs_predicted_user(test_input, test_output, model, output_scalers,
                                   path+'/test', output_labels)

    return


def my_postprocessing_FI(experiment_name_base, NN_input, NN_output, model_name, number_samples, results_path):
    # bug: we need to change the paths to load parts of the model if proceed
    experiment_name = experiment_name_base + '_%s' % (model_name)
    output_path = results_path + '/' + experiment_name + '_dataset%d' % number_samples
    architecture_json_filename = output_path + '/model_files/json/' + experiment_name + '_architecture.json'
    model_folder = output_path + '/model_files/weights/' + experiment_name
    last_epoch = search_last_epoch(output_path, experiment_name)
    model_h5_name = model_folder = output_path + '/model_files/h5/' + experiment_name + '_epoch_' + str(
        last_epoch) + '.h5'

    new_model = load_model(model_h5_name, custom_objects={'BilinearUpsampling1D': BilinearUpsampling1D,
                                                          'costum_sigmoid':costum_sigmoid},
                            compile=False)

    # evaluate the DNN on training, validation, and test datasets and save the results
    plot_ground_truth_vs_predicted_all_data_partitions_user(training_input=NN_input.train,
                                                           trainining_output=NN_output.train,
                                                           val_input=NN_input.validation,
                                                           val_output=NN_output.validation,
                                                           test_input=NN_input.test,
                                                           test_output=NN_output.test,
                                                           model=new_model,
                                                           path=output_path,
                                                           output_scalers=NN_output.scaler[0],
                                                           output_labels=NN_output.names[0])

    del new_model
    return


def plot_ground_truth_vs_predicted_user(input_scaled_values, true_scaled_output, my_model, output_scalers,
                                   my_output_path, output_labels):
    # evaluate the trained dnn inside the interval
    predicted= my_model.predict(input_scaled_values)

    #linear inverse rescale of predicted and truth values
    predicted_FI = linear_rescale_inverse(values=predicted[2], scalers=output_scalers)
    ground_truth = linear_rescale_inverse(values=true_scaled_output[0], scalers=output_scalers)
    print("plotting ground truth vs predicted values")
    # plot predicted values vs ground truth
    new_output_labels = [label + '_FI' for label in output_labels]
    plot_predicted_values_and_ground_truth_all_variable(predicted_values=predicted_FI,
                                                       ground_truth=ground_truth,
                                                       output_directory=my_output_path,
                                                       figure_labels=new_output_labels)
    write_ground_predicted_files(predicted[0], ground_truth, my_output_path, new_output_labels)
    return
