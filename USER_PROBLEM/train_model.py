# -*- coding: utf-8 -*-
# !/usr/bin/python
import sys

sys.path.append('.')
sys.path.append('../KERNEL')
import time

from DATA_READING_USER.read_data import load_my_data_to_train_NN
from DNN_TRAINING.training import generate_and_train_model
from DATA_PREPROCESSING.input_output import join_several_NN_dataset_classes, make_input_output_list
from DNN_TRAINING.training_tools import model_variables, training_variables
from DNN_TRAINING_USER.my_model import model_f,model_i, model_FI
from DNN_TRAINING_USER.my_callbacks import errors_I_user, save_last_models_user
from GENERATE_RESULTS.generate_tex import automatic_report
from DNN_ARCHITECTURES.architecture_tools import model_function_class, NN_class
from DNN_TRAINING.callbacks import show_basics, save_weights_and_optimizer_state_each_n_epochs, save_history, save_last_models,save_architecture
from POSTPROCESSING_USER.postprocess import my_postprocessing_submodel, my_postprocessing_FI
from DATA_PREPROCESSING.rescale import load_rescalers, linear_rescale_inverse
from tensorflow.keras.callbacks import ReduceLROnPlateau

import numpy
from DNN_TRAINING.training_tools import default_optimizer_list

if __name__ == "__main__":
    time_start = time.time()

    #########################################################################
    ###################          USER VARIABLES           ###################
    #########################################################################
    experiment_name_base = 'EXP_NAME'
    #
    author_of_the_experiment = "Author"
    generate_pdf = False

    # define relative paths
    dataset_path = 'PATH TO DATA'



    output_base_path = 'PATH To RESULTS'

    # structure of the dataset files
    dimension_input_files = [46, 2]
    dimension_output_files = [7]
    number_log_positions = 1
    # number of files: dataset used in train+validation+test
    number_max_files = 100

    # dataset percentage for training-validation-test
    percentage_train = 0.8
    percentage_val = 0.1
    percentage_test = 0.1

    # training variables
    train_var = training_variables()
    train_var.number_epochs = 1
    train_var.batch_size = 256
    train_var.save_each_n_epoch = 1
    # Retraining option:
    # Use the number of epoch that has to be loaded
    # Use "Last" to automatically search the last epoch
    # If it is not valid, automaticly a new model is generated
    train_var.retrain_from_epoch = "Last"
    #
    train_var.number_samples = number_max_files
    #
    # Define the list of callbacks
    #    train_var.callbacks =[show_basics()]
    #train_var.callbacks = get_callbacks_list()
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                  patience=5, verbose=0, mode='auto',
                                  epsilon=0.0001, cooldown=0, min_lr=0.1)


    train_var.callbacks = [save_weights_and_optimizer_state_each_n_epochs(), save_history(),
                           show_basics(), save_last_models_user(), errors_I_user(),
                           save_architecture(),reduce_lr]
    #
    train_var.optimizers = default_optimizer_list()

    ###################
    # Create lists to run several experiments.
    ###################

    model_variables = model_variables()
    model_variables.name      = ["exp0"]
    model_variables.function  = [model_FI]
    model_variables.optimizer = ['rmsprop']
    model_variables.norm_type = ["l1"]
    model_variables.loss = ['mae']
    model_variables.loss_weights = [None]
    model_variables.metrics =[['mae'] ]
    #This corresponds to this model [F, I, FI]
    model_variables.loss_weights=[[1,0,0]]


    #variables to configure the report. Inside the class automatic_report
    report = automatic_report()
    #Developed in other test
    report.generation = False
    experiment_name = experiment_name_base + '_%s' % (model_variables.name[0])
    scalers_directory = output_base_path + '/' + experiment_name + '_dataset%d' % train_var.number_samples + '/model_files/scalers/'
    #########################################################################
    # This function must be defined by the user to obtain the data
    # in the NN_input_output class structure
    #########################################################################
    rows_input = [64]
    all_rows_input = True
    columns_input = [0,1,46,47,30,31,8,9,10,11,44,45]

    all_columns_input = False
    all_rows_output = False
    rows_output = [0,1,2,4,5]
    # load data from file, split, reshape...
    try:
        # load the scalers
        scalers_input             = load_rescalers(scalers_dimension=len(columns_input),
                                                   scaler_filename=scalers_directory+'scaler_measurements')
        scalers_input_trajectory  = load_rescalers(scalers_dimension=2,
                                                   scaler_filename=scalers_directory+'scaler_trajectory_measurements')
        scalers_output            = load_rescalers(scalers_dimension=1,
                                                   scaler_filename=scalers_directory+'scaler_materials')
        scalers_output_trajectory = load_rescalers(scalers_dimension=1,
                                                   scaler_filename=scalers_directory+'scaler_materials_trajectory')
    except:
        scalers_input = None
        scalers_input_trajectory = None
        scalers_output = None
        scalers_output_trajectory = None

    measurements, measurements_trajectory, materials, materials_trajectory = \
        load_my_data_to_train_NN(dataset_directory=dataset_path,
                                  percentage_train=percentage_train,
                                  percentage_val=percentage_val,
                                  percentage_test=percentage_test,
                                  input_dim=dimension_input_files,
                                  output_dim=dimension_output_files,
                                  dataset_length=number_max_files,
                                  input_directory='INPUT',
                                  output_directory='OUTPUT',
                                  number_log_positions=number_log_positions,
                                  scaler_input=scalers_input,
                                  scaler_input_trajectory=scalers_input_trajectory,
                                  scaler_output=scalers_output,
                                  scaler_output_trajectory=scalers_output_trajectory,
                                  rows_input=rows_input,
                                  all_rows_input=all_rows_input,
                                  all_rows_output=all_rows_output,
                                  rows_output=rows_output,
                                  all_columns_input=all_columns_input,
                                  columns_input=columns_input,
                                  truncate_data=True)


    NN_input = make_input_output_list((materials, materials_trajectory,
                                       measurements, measurements_trajectory))

    NN_output = make_input_output_list((measurements,materials, measurements))

    #The variables needed to make the architecture
    NN_forward = NN_class()
    NN_forward.input_shape_m = materials.train.shape[1:]
    NN_forward.input_shape_trj = materials_trajectory.train.shape[1:]
    NN_forward.output_shape = measurements.train.shape[1:]
    NN_forward.name = "FORWARD"

    NN_inverse = NN_class()
    NN_inverse.input_shape_m = measurements.train.shape[1:]
    NN_inverse.input_shape_trj = measurements_trajectory.train.shape[1:]
    NN_inverse.output_shape = materials.train.shape[1:]
    NN_inverse.name = "INVERSE"

    M = model_function_class()
    # talk with Jon Ander. The usser can define here all their necessities
    M.forward, M.forward.input_layer_m, M.forward.input_layer_trj = model_f(NN_forward)

    M.inverse, M.inverse.input_layer_m, M.inverse.input_layer_trj = model_i(NN_inverse)


    ###########################################################################
    #
    #########################################################################
    # This function is in the Kernel. Only call it to train the NN!!
    #########################################################################
    generate_and_train_model(experiment_name_base, NN_input, NN_output, M, model_variables, train_var, report, output_base_path)
    #########################################################################

    measurement_trajectory_train = numpy.concatenate((NN_input.train[2], NN_input.train[3]), axis=2)
    measurement_trajectory_val = numpy.concatenate((NN_input.validation[2], NN_input.validation[3]), axis=2)
    measurement_trajectory_test = numpy.concatenate((NN_input.test[2], NN_input.test[3]), axis=2)


    material_trajectory_train = numpy.concatenate((NN_input.train[0], NN_input.train[1]), axis=1)
    material_trajectory_val = numpy.concatenate((NN_input.validation[0], NN_input.validation[1]), axis=1)
    material_trajectory_test = numpy.concatenate((NN_input.test[0], NN_input.test[1]), axis=1)


    my_postprocessing_submodel(experiment_name_base, NN_input_train=measurement_trajectory_train, NN_output_train=NN_input.train[0],
                               NN_input_validation=measurement_trajectory_val,NN_output_validation=NN_input.validation[0],
                               NN_input_test=measurement_trajectory_test, NN_output_test=NN_input.test[0],
                               NN_output_scaler=materials.scaler,NN_output_names=NN_input.names[0],
                               model_name=model_variables.name[0], submodel_name=NN_inverse.name,
                               number_samples=number_max_files, results_path=output_base_path)


    my_postprocessing_submodel(experiment_name_base, NN_input_train=material_trajectory_train, NN_output_train=NN_input.train[2],
                               NN_input_validation=material_trajectory_val,NN_output_validation=NN_input.validation[2],
                               NN_input_test=material_trajectory_test, NN_output_test=NN_input.test[2],
                               NN_output_scaler=measurements.scaler,NN_output_names=NN_input.names[2],
                               model_name=model_variables.name[0], submodel_name=NN_forward.name,
                               number_samples=number_max_files, results_path=output_base_path)
    #
    my_postprocessing_FI(experiment_name_base, NN_input, NN_output, model_variables.name[0], train_var.number_samples,
                      output_base_path)
    #########################################################################

    trajectory_train = linear_rescale_inverse(materials_trajectory.train, scalers=materials_trajectory.scaler)
    trajectory_train = (180.0 * trajectory_train) / numpy.pi

    trajectory_validation = linear_rescale_inverse(materials_trajectory.validation, scalers=materials_trajectory.scaler)
    trajectory_validation = (180.0 * trajectory_validation) / numpy.pi

    trajectory_test = linear_rescale_inverse(materials_trajectory.test, scalers=materials_trajectory.scaler)
    trajectory_test = (180.0 * trajectory_test) / numpy.pi

    numpy.savetxt(output_base_path + '/' + experiment_name + '_dataset%d' % train_var.number_samples+'/train/trajectories.csv',
                  trajectory_train[:,:], delimiter=",")

    numpy.savetxt(output_base_path + '/' + experiment_name + '_dataset%d' % train_var.number_samples+'/validation/trajectories.csv',
                  trajectory_validation[:,:], delimiter=",")

    numpy.savetxt(output_base_path + '/' + experiment_name + '_dataset%d' % train_var.number_samples+'/test/trajectories.csv',
                  trajectory_test[:,:], delimiter=",")

    time_end = time.time()
    print("End training_module:", time_end - time_start, "seconds")