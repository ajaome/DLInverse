import sys

sys.path.append('..')
sys.path.append('../../KERNEL')
import os
import numpy
from DATA_READING.read_data import load_train_val_test
from DATA_PREPROCESSING.rescale import rescale_forward, save_rescalers
from DATA_PREPROCESSING.input_output import NN_dataset
from scipy import stats
from termcolor import colored



def find_outliers(data, maximum_percentage_to_remove=0.01, initial_zscore=2):

    zscores = numpy.empty(data.shape)
    data_to_remove = numpy.zeros(data.shape,dtype=bool)

    for i in range(data.shape[2]):
        zscores[:, 0, i] = stats.zscore(data[:, 0, i])
    zscores = numpy.absolute(zscores)
    for i in range(data.shape[2]):
        z = initial_zscore
        data_percentage = 1
        while data_percentage >= maximum_percentage_to_remove:
            zscore_bound = numpy.zeros(data.shape[0])
            for j in range(zscores.shape[0]):
                if zscores[j, 0, i] >= z:
                    zscore_bound[j] = 1
            zscore_bound.astype(int)
            data_percentage = numpy.sum(zscore_bound)/data.shape[0]
            if data_percentage < maximum_percentage_to_remove:
                removing_indeces = zscore_bound.nonzero()
                data_to_remove[removing_indeces, 0, :] = 1

            z += 1
            if z > 10:
                print(colored('Something went wrong in the data cleaning: z score bigger than 10', 'red'))
                print(colored('Check the dataset for the following column of the data', 'green'), colored(i, 'green'))
                print(colored('See function find_outliers for more details', 'blue'))
                sys.exit()

    outliers=numpy.where(data_to_remove[:,0,0])

    return outliers

def remove_outliers(input_data, input_data_traj,output_data, output_data_traj):
    outliers = find_outliers(data=input_data, maximum_percentage_to_remove=0.01, initial_zscore=3)
    primary_size = input_data.shape[0]
    input_data = numpy.delete(input_data, outliers, 0)
    output_data = numpy.delete(output_data, outliers, 0)
    input_data_traj = numpy.delete(input_data_traj, outliers, 0)
    output_data_traj = numpy.delete(output_data_traj, outliers, 0)

    if (primary_size-input_data.shape[0])/primary_size > 0.1:
        print(colored('Something went wrong in the data cleaning: more than 10% of data was cleaned', 'red'))
        print(colored('See function remove_outliers for more details', 'blue'))
        sys.exit()

    print(colored('Number of outliers to be removed', 'green'), colored(primary_size-input_data.shape[0], 'green'))
    print(colored('removed oulier samples are', 'blue'), colored(outliers, 'blue'))

    return input_data, input_data_traj, output_data, output_data_traj, outliers



def build_input_output_dlinverse(inputs, outputs, dataset_size, number_log_positions, train_dataset=True,
                                     scaler_measurements=None, scaler_measurements_trajectory=None,
                                     scaler_materials=None, scaler_materials_trajectory=None,
                                     all_rows_input=True, rows_input=[],
                                     all_columns_input=True, columns_input=[],
                                     all_rows_output=True, rows_output=[],
                                     rescale_data=True, truncate_data=True, couple_measurements=True):
    # initialize the input output matrixes
    number_of_measurements = int((inputs.shape[1] - 1) / number_log_positions)
    # dataset input
    measurements = numpy.zeros([dataset_size, number_log_positions, number_of_measurements * 2])
    measurements_trajectory = numpy.zeros([dataset_size, number_log_positions, 2])
    # dataset output
    materials = numpy.zeros([dataset_size, outputs.shape[1]])
    materials_trajectory = numpy.zeros([dataset_size, 2])

    # loop through all samples
    for isample in range(dataset_size):
        # output_dataset
        materials[isample, 0:outputs.shape[1]] = outputs[isample, :]

        # build the trajectory info radians conversion
        trajectory_final = (numpy.pi * inputs[isample, 0, 0]) / 180.0
        trajectory_deviation = (numpy.pi * inputs[isample, 0, 1]) / 180.0
        materials_trajectory[isample, 0] = trajectory_final
        materials_trajectory[isample, 1] = trajectory_deviation

        # input_dataset
        for iposition in range(number_log_positions):
            trajectory_current = trajectory_final - (number_log_positions - 1 - iposition) * trajectory_deviation
            measurements_trajectory[isample, iposition, 0] = numpy.sin(trajectory_current)
            measurements_trajectory[isample, iposition, 1] = numpy.cos(trajectory_current)

        # build the measurements dataset
        for imeasurement in range(number_of_measurements):
            measurements[isample, :, 2*imeasurement:(2*imeasurement)+2] = \
                inputs[isample, (imeasurement*number_log_positions) +1:((imeasurement+1)*number_log_positions) + 1, 0:2]

    #select the desired rows for the input(measurements)
    if all_rows_input:
        #select the number of measurements required
        if all_columns_input:
            measurements_rescaled = numpy.zeros([measurements.shape[0],
                                                 measurements.shape[1], measurements.shape[2]])

            measurements_rescaled[:, :, :] = measurements[:, :, :]

        else:
            measurements_rescaled = numpy.zeros([measurements.shape[0],
                                                 measurements.shape[1], len(columns_input)])

            measurements_rescaled[:, :, :] = measurements[:, :, columns_input]

        measurements_trajectory_rescaled = numpy.zeros([measurements_trajectory.shape[0],
                                                        measurements_trajectory.shape[1],
                                                        measurements_trajectory.shape[2]])

        measurements_trajectory_rescaled[:, :, :] = measurements_trajectory[:, :, :]
    else:

        if all_columns_input:
            measurements_rescaled = numpy.zeros([measurements.shape[0], len(rows_input),
                                                 measurements.shape[2]])
            for i in range(measurements.shape[0]):
                for j in range(len(rows_input)):
                    measurements_rescaled[i, j, :] = measurements[i, rows_input[j], :]

        else:
            measurements_rescaled = numpy.zeros([measurements.shape[0], len(rows_input),
                                                 len(columns_input)])
            for i in range(measurements.shape[0]):
                for j in range(len(rows_input)):
                    measurements_rescaled[i, j, :] = measurements[i, rows_input[j], columns_input]

        measurements_trajectory_rescaled = numpy.zeros([measurements_trajectory.shape[0], len(rows_input),
                                                        measurements_trajectory.shape[2]])

        for i in range(measurements.shape[0]):
            for j in range(len(rows_input)):
                measurements_trajectory_rescaled[i, j, :] = measurements_trajectory[i, rows_input[j], :]


    log_labels_measurements = [0] * measurements_rescaled.shape[2]


    names_materials = [r'rho_u', r'rho_l', r'rho_h', r'a', r'd_u', r'd_l', r'beta']
    #select the desired rows for the output(materials)
    if all_rows_output:
        materials_rescaled = numpy.zeros([materials.shape[0], materials.shape[1]])
        materials_rescaled[:, :] = materials[:, :]
        names_selected_materials = [r'rho_u', r'rho_l', r'rho_h', r'a', r'd_u', r'd_l', r'beta']
        log_labels_materials = [0]*materials.shape[1]

    else:
        materials_rescaled = numpy.zeros([materials.shape[0], len(rows_output)])
        names_selected_materials=[]
        log_labels_materials = [0] * len(rows_output)

        for i in range(materials.shape[0]):
            for j in range(len(rows_output)):
                materials_rescaled[i, j] = materials[i, rows_output[j]]

        for j in range(len(rows_output)):
            names_selected_materials.append(names_materials[rows_output[j]])


    if number_log_positions==1:
        materials_trajectory_rescaled = numpy.zeros([materials.shape[0], 1])
        materials_trajectory_rescaled[:,0] = materials_trajectory[:,0]
    else:
        materials_trajectory_rescaled = numpy.zeros([materials.shape[0], 2])
        materials_trajectory_rescaled[:,:] = materials_trajectory[:,:]

    log_labels_materials_trajectory = [0]*materials_trajectory_rescaled.shape[1]


    if truncate_data is True:
        measurements_rescaled, measurements_trajectory_rescaled,\
        materials_rescaled, materials_trajectory_rescaled,_ = \
            remove_outliers(measurements_rescaled, measurements_trajectory_rescaled,
                                    materials_rescaled, materials_trajectory_rescaled)

    # scale the dataset into the interval:
    scaler_interval = [0.5, 1.5]
    # define names and rescale type
    # Put "0" for linear- and "1" for logarithmic-scale in log_labels
    names_measurements=[]
    if couple_measurements:
        for imeasurement in range(number_of_measurements):
            names_measurements.append("atten_"+str(imeasurement+1))
            names_measurements.append("phase_"+str(imeasurement+1))
    else:
        for imeasurement in range(2*number_of_measurements):
            names_measurements.append("M_"+str(imeasurement+1))

    names_measurements_trajectory = [r"sin(\thetha)", r"cos(\thetha)"]
    log_labels_measurements_trajectory = [0, 0]
    names_materials_trajectory = ["final trajectory", "deviation trajectory"]

    if rescale_data is True:
        scaler_measurements, measurements_rescaled[:, :, 0:measurements_rescaled.shape[2]] = \
            rescale_forward(values=measurements_rescaled[:, :, 0:measurements_rescaled.shape[2]],
                            scaler=scaler_measurements,
                            log_labels=log_labels_measurements,
                            scaler_interval=scaler_interval)

        scaler_measurements_trajectory, measurements_trajectory_rescaled[:, :, 0:measurements_trajectory_rescaled.shape[2]] = \
            rescale_forward(values=measurements_trajectory_rescaled[:, :, 0:measurements_trajectory_rescaled.shape[2]],
                            scaler=scaler_measurements_trajectory,
                            log_labels=log_labels_measurements_trajectory,
                            scaler_interval=scaler_interval)

        scaler_materials, materials_rescaled[:, 0:materials_rescaled.shape[1]] = \
            rescale_forward(values=materials_rescaled[:, 0:materials_rescaled.shape[1]],
                            scaler=scaler_materials,
                            log_labels=log_labels_materials,
                            scaler_interval=scaler_interval)

        scaler_materials_trajectory, materials_trajectory_rescaled[:, 0:materials_trajectory_rescaled.shape[1]] = \
            rescale_forward(values=materials_trajectory_rescaled[:, 0:materials_trajectory_rescaled.shape[1]],
                            scaler=scaler_materials_trajectory,
                            log_labels=log_labels_materials_trajectory,
                            scaler_interval=scaler_interval)

    # save the scalers only once
    if rescale_data is True:
        if train_dataset:
            try:
                os.makedirs('./scalers')
            except:
                pass
            save_rescalers(scalers=scaler_materials, scaler_filename='./scalers/scaler_materials')
            save_rescalers(scalers=scaler_materials_trajectory, scaler_filename='./scalers/scaler_materials_trajectory')
            save_rescalers(scalers=scaler_measurements, scaler_filename='./scalers/scaler_measurements')
            save_rescalers(scalers=scaler_measurements_trajectory,
                           scaler_filename='./scalers/scaler_trajectory_measurements')

    return names_measurements, log_labels_measurements, scaler_measurements, measurements_rescaled, \
           names_measurements_trajectory, log_labels_measurements_trajectory, scaler_measurements_trajectory, measurements_trajectory_rescaled, \
           names_selected_materials, log_labels_materials, scaler_materials, materials_rescaled, \
           names_materials_trajectory, log_labels_materials_trajectory, scaler_materials_trajectory, materials_trajectory_rescaled


def load_my_data_to_train_NN(dataset_directory, percentage_train, percentage_val, percentage_test,
                              input_dim, output_dim, dataset_length=None, random_state=66,
                              input_directory='INPUT', output_directory='OUTPUT',
                              number_log_positions=65, scaler_interval=[0.5, 1.5],
                              scaler_input=None, scaler_input_trajectory=None,
                              scaler_output=None, scaler_output_trajectory=None,
                              all_rows_input=True, rows_input=[64],
                              all_columns_input=True, columns_input=[],
                              all_rows_output=True, rows_output=[0],
                              rescale_data=True, truncate_data=True,couple_measurements=True):
    # split (train-validation-test) and load the data
    input_train, output_train, input_val, output_val, input_test, output_test = \
        load_train_val_test(dataset_directory=dataset_directory,
                            percentage_train=percentage_train,
                            percentage_val=percentage_val,
                            percentage_test=percentage_test,
                            input_dim=input_dim,
                            output_dim=output_dim,
                            dataset_length=dataset_length,
                            random_state=random_state,
                            input_directory=input_directory,
                            output_directory=output_directory)


    # initialize the structure data for each data type
    measurements = NN_dataset()
    measurements_trajectory = NN_dataset()
    materials = NN_dataset()
    materials_trajectory = NN_dataset()

    measurements.scaler = scaler_input
    measurements_trajectory.scaler = scaler_input_trajectory
    materials.scaler = scaler_output
    materials_trajectory.scaler = scaler_output_trajectory
    #################

    # identificate, reshape, and scale the train-validation-test for each data type
    # train dataset
    if (percentage_train!=0):
        measurements.names, measurements.log_label, measurements.scaler, measurements.train, \
        measurements_trajectory.names, measurements_trajectory.log_label, measurements_trajectory.scaler, measurements_trajectory.train, \
        materials.names, materials.log_label, materials.scaler, materials.train, \
        materials_trajectory.names, materials_trajectory.log_label, materials_trajectory.scaler, materials_trajectory.train = \
            build_input_output_dlinverse(inputs=input_train, outputs=output_train,
                                             dataset_size=input_train.shape[0],
                                             number_log_positions=number_log_positions,
                                             train_dataset=True,
                                             scaler_measurements=measurements.scaler,
                                             scaler_measurements_trajectory=measurements_trajectory.scaler,
                                             scaler_materials=materials.scaler,
                                             scaler_materials_trajectory=materials_trajectory.scaler,
                                             all_rows_input=all_rows_input,
                                             rows_input=rows_input,
                                             all_columns_input=all_columns_input,
                                             columns_input=columns_input,
                                             all_rows_output=all_rows_output,
                                             rows_output=rows_output,
                                             rescale_data=rescale_data,
                                             truncate_data=truncate_data,
                                             couple_measurements=couple_measurements)

    if (percentage_val != 0):
        # validation dataset
        measurements.names, measurements.log_label, measurements.scaler, measurements.validation, \
        measurements_trajectory.names, measurements_trajectory.log_label, measurements_trajectory.scaler, measurements_trajectory.validation, \
        materials.names, materials.log_label, materials.scaler, materials.validation, \
        materials_trajectory.names, materials_trajectory.log_label, materials_trajectory.scaler, materials_trajectory.validation = \
            build_input_output_dlinverse(inputs=input_val, outputs=output_val,
                                             dataset_size=input_val.shape[0],
                                             number_log_positions=number_log_positions,
                                             train_dataset=False,
                                             scaler_measurements=measurements.scaler,
                                             scaler_measurements_trajectory=measurements_trajectory.scaler,
                                             scaler_materials=materials.scaler,
                                             scaler_materials_trajectory=materials_trajectory.scaler,
                                             all_rows_input=all_rows_input,
                                             rows_input=rows_input,
                                             all_columns_input=all_columns_input,
                                             columns_input=columns_input,
                                             all_rows_output=all_rows_output,
                                             rows_output=rows_output,
                                             rescale_data=rescale_data,
                                             truncate_data=truncate_data,
                                             couple_measurements=couple_measurements)
    if (percentage_test != 0):
        # test dataset
        measurements.names, measurements.log_label, measurements.scaler, measurements.test, \
        measurements_trajectory.names, measurements_trajectory.log_label, measurements_trajectory.scaler, measurements_trajectory.test, \
        materials.names, materials.log_label, materials.scaler, materials.test, \
        materials_trajectory.names, materials_trajectory.log_label, materials_trajectory.scaler, materials_trajectory.test = \
            build_input_output_dlinverse(inputs=input_test, outputs=output_test,
                                             dataset_size=input_test.shape[0],
                                             number_log_positions=number_log_positions,
                                             train_dataset=False,
                                             scaler_measurements=measurements.scaler,
                                             scaler_measurements_trajectory=measurements_trajectory.scaler,
                                             scaler_materials=materials.scaler,
                                             scaler_materials_trajectory=materials_trajectory.scaler,
                                             all_rows_input=all_rows_input,
                                             rows_input=rows_input,
                                             all_columns_input=all_columns_input,
                                             columns_input=columns_input,
                                             all_rows_output=all_rows_output,
                                             rows_output=rows_output,
                                             rescale_data=rescale_data,
                                             truncate_data=truncate_data,
                                             couple_measurements=couple_measurements)

    return measurements, measurements_trajectory, materials, materials_trajectory
