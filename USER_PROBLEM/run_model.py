# -*- coding: utf-8 -*-
# !/usr/bin/python
from tensorflow.keras.models import load_model
import os
import sys
sys.path.append('.')
sys.path.append('../KERNEL')
import time
import numpy
from DNN_EVALUATE.evaluate_model import evaluate_dnn_real_scale
from DNN_TRAINING_USER.my_model import BilinearUpsampling1D, costum_sigmoid
from DATA_READING_USER.read_data import load_my_data_to_train_NN
from GENERATE_RESULTS.write_results_into_files import write_results_in_csv_file
from DATA_PREPROCESSING.rescale import load_rescalers
from POSTPROCESSING_USER.postprocess import write_measurements_into_dat_file


def run_trained_model_dlinverse(model_filename, scalers_input_filename, scalers_input_dim,
                                    scalers_input_trajectory_filename, scalers_input_trajectory_dim,
                                    scalers_output_filename, scalers_output_dim,
                                    scalers_output_trajectory_filename, scalers_output_trajectory_dim,
                                    dataset_directory, dataset_length, nr_logging_positions,
                                    input_directory='INPUT', input_dim=[195,2],
                                    output_directory='OUTPUT', output_dim=[7],
                                    problem_type='inverse',
                                    results_directory='./results_trained_model',
                                    results_file_name='results',
                                    all_rows_input=True,
                                    rows_input=[64],
                                    all_rows_output=True,
                                    rows_output=[0],
                                    all_columns_input=True,
                                    columns_input=[],
                                    truncate_data=False):
    # make directory for the results
    try:
        os.makedirs(results_directory)
    except:
        pass

    # load the scalers
    scalers_input             = load_rescalers(scalers_dimension=scalers_input_dim,
                                               scaler_filename=scalers_input_filename)
    scalers_input_trajectory  = load_rescalers(scalers_dimension=scalers_input_trajectory_dim,
                                               scaler_filename=scalers_input_trajectory_filename)
    scalers_output            = load_rescalers(scalers_dimension=scalers_output_dim,
                                               scaler_filename=scalers_output_filename)
    scalers_output_trajectory = load_rescalers(scalers_dimension=scalers_output_trajectory_dim,
                                               scaler_filename=scalers_output_trajectory_filename)

    # load the dataset
    measurements, measurements_trajectory, materials, materials_trajectory = \
        load_my_data_to_train_NN(dataset_directory=dataset_directory,
                                          percentage_train=1.,
                                          percentage_val=0.0,
                                          percentage_test=0.0,
                                          input_dim=input_dim,
                                          output_dim=output_dim,
                                          dataset_length=dataset_length,
                                          input_directory=input_directory,
                                          output_directory=output_directory,
                                          number_log_positions=nr_logging_positions,
                                          scaler_input=scalers_input,
                                          scaler_input_trajectory=scalers_input_trajectory,
                                          scaler_output=scalers_output,
                                          scaler_output_trajectory=scalers_output_trajectory,
                                          all_rows_input=all_rows_input,
                                          rows_input=rows_input,
                                          all_rows_output=all_rows_output,
                                          rows_output=rows_output,
                                          all_columns_input=all_columns_input,
                                          columns_input=columns_input,
                                          truncate_data=truncate_data)


    model = load_model(model_filename, custom_objects={'BilinearUpsampling1D': BilinearUpsampling1D,
                                                       'costum_sigmoid':costum_sigmoid}, compile=False)


    if (problem_type == 'forward'):
        model.summary()
        NNinput = numpy.concatenate((materials.train, materials_trajectory.train), axis=-1)
        log_labels = [0, 0, 0, 0, 0, 0]
        last_pos_predicted=numpy.empty([measurements.train.shape[0],measurements.train.shape[2]])
        predicted_values = evaluate_dnn_real_scale(input_values=NNinput,
                                                   dnn_model=model,
                                                   scaler=scalers_input,
                                                   log_labels=log_labels)

        for isample in range(measurements.train.shape[0]):
            last_pos_predicted[isample,:] = predicted_values[isample, measurements.train.shape[1]-1, :]


        measurements_type = ['Atten-Azim', 'Phase-Azim', 'Atten-Geosignal',
                             'Phase-Geosignal', 'Atten-Coaxial', 'Phase-Coaxial']

        write_measurements_into_dat_file(last_pos_predicted, results_directory+'/'+results_file_name)
        # numpy.savetxt('r.dat', predicted_values[0,:,:])
        write_results_in_csv_file(values=predicted_values, results_directory=results_directory,
                                  csv_filename='predicted_values', labels=measurements_type)

    else:
        NNinput = numpy.concatenate((measurements.train, measurements_trajectory.train), axis=-1)
        if all_rows_output is True:
            log_labels = [0, 0, 0, 0, 0, 0, 0]
            predicted_values = evaluate_dnn_real_scale(input_values=NNinput,
                                                          dnn_model=model,
                                                          scaler=scalers_output,
                                                          log_labels=log_labels)

        else:
            log_labels = [0] * len(rows_output)
            predicted_values_part = evaluate_dnn_real_scale(input_values=NNinput,
                                                            dnn_model=model,
                                                            scaler=scalers_output,
                                                            log_labels=log_labels)

            predicted_values = numpy.zeros([predicted_values_part.shape[0], 7])

            for row in range(len(rows_output)):
                predicted_values[:, rows_output[row]] = predicted_values_part[:, row]


        write_measurements_into_dat_file(predicted_values, results_directory + '/materials.dat')

        # Write predicted values for training dataset in a csv file
        write_results_in_csv_file(values=predicted_values,
                                  results_directory=results_directory,
                                  csv_filename='Results_pred')

    return predicted_values


def evaluate_model():
    # initialize the directories of inputs and outputs
    example_directory = '/Users/mostafashahriari/Codes/1D_AC_INVERSION_copy/EXEC/MOSTAFA_DL_SYNTHETIC_2_AR1/'
    results_directory = '/Users/mostafashahriari/Codes/1D_AC_INVERSION_copy/EXEC/MOSTAFA_DL_SYNTHETIC_2_AR1/DL/'
    dataset_directory = example_directory + 'DL'
    real_data_directory = example_directory + 'OUTPUT/Trajectory_well_01iteration_00.vtk'
    results_file_name = 'MOSTAFA_DL_SYNTHETIC_1_AR1.dat'
    input_directory = 'INPUT'
    output_directory = 'OUTPUT'
    #dataset_length = 752

    # initialize info about the dataset
    dataset_length = 1799
    nr_logging_positions = 1
    input_dim = [46, 2]
    # columns_input = [0, 1,8,9,48,49, 80, 81,36,37,42]
    # columns_input = [0,1,46,47,30,31,8,9,10,11,44,45]
    # columns_input = [0,1,46,47,40,30,31,8,9,44,45,10,11]
    columns_input = [0,1,46,47,40,30,31,8,9,44,45,10,11]
    all_columns_input = False
    output_dim = [7]
    rows_input = [64]
    all_rows_input = True
    all_rows_output = False
    rows_output = [0,1,2,4,5]

    truncate_data = False

    # the direction to the trained models and scalers
    trained_model_directory = '/Users/mostafashahriari/Desktop/Inverse_full_measurements_Final_dataset300000/model_files/h5/'
    scaler_directory = '/Users/mostafashahriari/Desktop/Inverse_full_measurements_Final_dataset300000/model_files/scalers/'
    model_filename_forward = trained_model_directory + 'Inverse_exp07_FORWARD_epoch_2200.h5'
    model_filename_inverse = trained_model_directory + 'Inverse_full_measurements_Final_INVERSE_epoch_3000.h5'
    model_filename_FI = trained_model_directory + 'Inverse_exp07_epoch_2200.h5'
    scalers_filename_input = scaler_directory + 'scaler_measurements'
    scalers_input_dim = len(columns_input)
    scalers_filename_input_trajectory = scaler_directory + 'scaler_trajectory_measurements'
    scalers_input_trajectory_dim = 2
    scalers_filename_output = scaler_directory + 'scaler_materials'
    scalers_output_dim = 1
    scalers_filename_output_trajectory = scaler_directory + 'scaler_materials_trajectory'
    scalers_output_trajectory_dim = 1

    # problem type
    problem_type='inverse'

    # evaluate the model
    run_trained_model_dlinverse(model_filename=model_filename_inverse,
                                    scalers_input_filename=scalers_filename_input,
                                    scalers_input_dim=scalers_input_dim,
                                    scalers_input_trajectory_filename=scalers_filename_input_trajectory,
                                    scalers_input_trajectory_dim=scalers_input_trajectory_dim,
                                    scalers_output_filename=scalers_filename_output,
                                    scalers_output_dim=scalers_output_dim,
                                    scalers_output_trajectory_filename=scalers_filename_output_trajectory,
                                    scalers_output_trajectory_dim=scalers_output_trajectory_dim,
                                    dataset_directory=dataset_directory,
                                    dataset_length=dataset_length,
                                    nr_logging_positions=nr_logging_positions,
                                    input_directory=input_directory,
                                    input_dim=input_dim,
                                    output_directory=output_directory,
                                    output_dim=output_dim,
                                    problem_type=problem_type,
                                    results_directory=results_directory,
                                    results_file_name=results_file_name,
                                    all_rows_input=all_rows_input,
                                    rows_input=rows_input,
                                    all_rows_output=all_rows_output,
                                    rows_output=rows_output,
                                    all_columns_input=all_columns_input,
                                    columns_input=columns_input,
                                    truncate_data=truncate_data)

    try:
        # if the measurements exist as a vtk file as output of the semi-analytic, we read it and pack them all in
        # one dat file to read in latex
        read_vtk_file_measurements(file_real=real_data_directory, file_predicted=results_directory+'/'+results_file_name,
                               results_file=results_directory+'/'+results_file_name+'tex.dat')
    except:
        pass

if __name__ == "__main__":
    time_start = time.time()

    evaluate_model()
    time_end = time.time()

    print ("End run:", time_end - time_start, "seconds")