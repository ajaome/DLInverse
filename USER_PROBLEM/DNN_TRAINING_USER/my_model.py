# -*- coding: utf-8 -*-
import tensorflow as tf
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import concatenate, Reshape, Dense, Input, Flatten, Add, Conv1D, UpSampling1D, MaxPooling1D,TimeDistributed
import tensorflow.keras.backend as K
from tensorflow.keras.initializers import glorot_normal, glorot_uniform
from tensorflow.python.ops import nn

class BilinearUpsampling1D(Layer):
    """
    Wrapping 1D BilinearUpsamling as a Keras layer
    Input: 3D Tensor (batch, dim, channels)
    """

    def __init__(self, size=65, **kwargs):
        self.size = size
        super(BilinearUpsampling1D, self).__init__(**kwargs)

    def build(self, input_shape):
        super(BilinearUpsampling1D, self).build(input_shape)

    def call(self, x, mask=None):
        x = K.expand_dims(x, axis=2)
        x = tf.image.resize(x, [self.size, 1])
        x = K.squeeze(x, axis=2)
        return x

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.size, input_shape[2])

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.size, input_shape[2])

    def get_config(self):
        config = {'size': self.size}
        base_config = super(BilinearUpsampling1D, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def generate_model_F(input_shape, output_shape, pool_length=2, nb_filter=40,
                     num_blocks=5, blocks_per_maxpooling=1):

    input_ = tf.keras.Input(input_shape)
    x=input_
    x = Reshape((1, input_shape[0]))(x)
    for i in range(num_blocks):
        id_block = Conv1D(filters=nb_filter*(i+1), kernel_size=3, padding='same',name='block_identity_%d' %i)(x)
        a = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_A_%d' %i)(x)
        d = Conv1D(filters=nb_filter*(i+1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_normal(), padding='same',name='block_deconv_B_%d' %i)(a)
        x = Add()([id_block, d])
        if (i + 1) % blocks_per_maxpooling == 0:
            x = UpSampling1D(pool_length)(x)

    x = BilinearUpsampling1D(output_shape[0])(x)
    outputs = Conv1D(filters=output_shape[-1], kernel_size=1, activation=costum_sigmoid,
                     kernel_initializer=glorot_normal(), padding='same', name='final_deconv')(x)
    model = tf.keras.Model(inputs=input_, outputs=outputs, name='FORWARD')
    model.summary()
    return model


def model_f (NN,trained_model_path=None):
    input_shape = (NN.input_shape_m[0]+NN.input_shape_trj[0],)
    input_m = tf.keras.Input(NN.input_shape_m)
    input_trj = tf.keras.Input(NN.input_shape_trj)
    model = generate_model_F(input_shape=input_shape,
                             output_shape=NN.output_shape)

    if trained_model_path is not None:
        model = tf.keras.models.load_model(trained_model_path,
                                           custom_objects={'BilinearUpsampling1D':BilinearUpsampling1D, 'costum_sigmoid':costum_sigmoid})
        model.summary()
        model.trainable = False

    model.summary()
    return model, input_m, input_trj

def generate_model_I(input_shape,output_shape, pool_length=2, nb_filter=40,num_blocks=6, blocks_per_maxpooling=7):

    input_ = Input(shape=input_shape)
    x = input_
    for i in range(num_blocks):
        idn_block = Conv1D(filters=nb_filter * (i + 1), kernel_size=3, padding='same', name='block_identity_%d' % i)(x)
        a = Conv1D(filters=nb_filter * (i + 1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_uniform(), padding='same', name='block_conv_A_%d' % i)(x)
        d = Conv1D(filters=nb_filter * (i + 1), kernel_size=3, activation='relu',
                   kernel_initializer=glorot_uniform(), padding='same', name='block_conv_B_%d' % i)(a)
        x = Add()([idn_block, d])
        # x = Dropout(0.25)(x)
        if (i + 1) % blocks_per_maxpooling == 0:
            x = MaxPooling1D(pool_length)(x)

    x = Flatten()(x)
    outputs = Dense(output_shape[0], kernel_initializer=glorot_uniform(),activation=costum_sigmoid, name='classification_layer')(x)
    model = tf.keras.Model(inputs=[input_], outputs=outputs, name='INVERSE')
    model.summary()
    return model

def model_i (NN):
    input_m = tf.keras.Input(NN.input_shape_m)
    input_trj = tf.keras.Input(NN.input_shape_trj)
    input_shape = (NN.input_shape_m[0], NN.input_shape_m[1]+NN.input_shape_trj[1])

    model = generate_model_I(input_shape=input_shape,
                             output_shape=NN.output_shape)
    model.summary()
    return model, input_m, input_trj

def model_FI(NN):

    F = NN.forward(concatenate([NN.forward.input_layer_m, NN.forward.input_layer_trj]))
    I = NN.inverse(concatenate([NN.inverse.input_layer_m,NN.inverse.input_layer_trj]))
    FI = NN.forward(concatenate([NN.inverse(concatenate([NN.inverse.input_layer_m,NN.inverse.input_layer_trj])),
                                 NN.forward.input_layer_trj]))     # F_h^dagger(y)-x

    model = tf.keras.Model(inputs=[NN.forward.input_layer_m, NN.forward.input_layer_trj,
                                   NN.inverse.input_layer_m, NN.inverse.input_layer_trj],
                           outputs=[F, I, FI])

    model.submodels = [NN.forward, NN.inverse]
    model.summary()
    return model

def model_forward(NN):
    model= NN.forward
    return model


def costum_sigmoid(x):
  return nn.sigmoid(x)+0.5