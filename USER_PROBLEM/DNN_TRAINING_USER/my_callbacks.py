import numpy
import os
from tensorflow.keras.callbacks import Callback, ModelCheckpoint, TensorBoard, ReduceLROnPlateau, LearningRateScheduler
from KERNEL.GENERATE_RESULTS.generate_results import evaluate_realative_and_absolute_errors_all_data_partitions
from KERNEL.GENERATE_RESULTS.generate_results import save_error_header,save_error_line
from KERNEL.GENERATE_RESULTS.generate_graphics import plot_new_error


class errors_I_user(Callback):
    # generate the files with header and previous epochs information
    def on_train_begin(self, logs=None):
        variable_names = self.model.x_names[0][0:7]
        training_error_path = self.model.experiment_folder + '/train/'
        validation_error_path = self.model.experiment_folder + '/validation/'
        files = [training_error_path + "relative_error.dat", training_error_path + "absolute_error.dat",
                 validation_error_path + "relative_error.dat", validation_error_path + "absolute_error.dat"]
        for fname in files:
            try:
                # load old data
                old_values = numpy.loadtxt(fname, delimiter="\t", skiprows=1)
                # write the new file
                save_error_header(fname, variable_names)
                for line in old_values:
                    epoch = line[0]
                    save_error_line(fname, epoch, line[1:])
                    if (epoch == self.model.start_epoch):
                        break
            except:
                save_error_header(fname, variable_names)

        return

    # evaluate each error type for one epoch and append them in the files
    def on_epoch_end(self, epoch, logs=None):
        training_error_path = self.model.experiment_folder + '/train/'
        validation_error_path = self.model.experiment_folder + '/validation/'
        # compute errors
        # bug: we need select the norm-type here!!!
        measurement_trajectory_train = numpy.concatenate((self.model.x_train[2],self.model.x_train[3]),axis=2)
        measurement_trajectory_val = numpy.concatenate((self.model.x_validation[2],self.model.x_validation[3]),axis=2)
        absololute_error_training, relative_error_training, \
        absololute_error_validation, relative_error_validation = \
            evaluate_realative_and_absolute_errors_all_data_partitions(self.model.submodels[1],
                                                                       measurement_trajectory_train,
                                                                       self.model.y_train[1],
                                                                       measurement_trajectory_val,
                                                                       self.model.y_validation[1])
        # append in the file
        save_error_line(training_error_path + 'absolute_error.dat', epoch, absololute_error_training)
        save_error_line(training_error_path + 'relative_error.dat', epoch, relative_error_training)
        save_error_line(validation_error_path + 'absolute_error.dat', epoch, absololute_error_validation)
        save_error_line(validation_error_path + 'relative_error.dat', epoch, relative_error_validation)

        return

    def on_train_end(self, logs={}):
        print("*****Saving error plots...")
        variable_names = self.model.x_names[0][0:7]
        training_error_path = self.model.experiment_folder + '/train/'
        validation_error_path = self.model.experiment_folder + '/validation/'
        #
        plot_new_error(self.model.experiment_folder, training_error_path, validation_error_path,
                       'absolute_error',variable_names)
        plot_new_error(self.model.experiment_folder, training_error_path, validation_error_path,
                       'relative_error',variable_names)
        return


class save_last_models_user(Callback):

    def on_train_begin(self, logs):
        try:
            os.makedirs(self.model.experiment_folder + '/model_files/h5')
        except:
            pass
        return


    def on_epoch_end(self, epoch,logs):
        if (epoch % int(self.model.save_each_n_epoch) == 0):
            print("***Saving model..." + self.model.experiment_name)
            path = self.model.experiment_folder + '/model_files/h5/' + self.model.experiment_name
            self.model.save(path +'_epoch_' + str(epoch) +'.h5')
            try:
                for submodel in self.model.submodels:
                    print ("***Saving submodel " + submodel.name)
                    submodel.save(path +'_'+ submodel.name +'_epoch_' + str(epoch) +'.h5')
            except:
                pass
        return
